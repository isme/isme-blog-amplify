S3 server access logging is not appropiate for auditible access to S3 buckets
because it doesn't guarantee to log everything and it's incompatible with S3
object lock.

CloudTrail's S3 data events feature appears to avoid these limitations.

---

A quick and dirty way to run any bash commands in a loop until you cancel it is
to run the whole thing in watch.

For example, to simulate repeated uploads to bucket to test S3 server access
logging or CloudTrail data events:

```bash
watch bash -c '
stamp="$(date --iso-8601=seconds)"

export AWS_PROFILE=...

echo "${stamp}" > ~/tmp/stamp

aws s3api put-object \
--bucket ... \
--key "test/${stamp}" \
--body ~/tmp/stamp
'
```

--- 

Use Terraform's depends_on to avoid deployment errors from dependencies that
Terraform can't derive, such as CloudTrail's dependency on an appropriate bucket
policy.
