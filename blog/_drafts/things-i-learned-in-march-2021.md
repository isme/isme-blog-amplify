I learned to analyze trace the root user credentials for an organization of 40 AWS accounts.

I learned to use TPM (Team Password Manager).

I learned to use ServiceNow.

I learned to use the Azure Devops CLI.

I learned to set up AWS SES to store incoming emails in an S3 bucket and to
notify of their receipt via SNS. I did this using Terraform.

I learned to use ServiceNow dashboards :-(

---

I learned to use git flow branching model.

And to really finish the hotfix you need to push three things. Am I missing
something, or why is this not automated by the command?

The solution might be to use `--push` option, but if you forget to use it you
need to do it manually like this:

```bash
git push origin master
git push origin develop
git push origin --tags
```

Thanks to
[James M Greene's breakdown of gitflow commands](https://gist.github.com/JamesMGreene/cdd0ac49f90c987e45ac).


---

Seminario de Spain.AI con Ezequiel Paura.

https://www.youtube.com/watch?v=81tP8V8hbJs

Demo:

* Amazon Sagemaker
* spaCy
* Hugging Face
* Docker

# Pregunta

GPT-3 tiene más de 1.000.000.000 parámetros.

Según se mida el español y el inglés cada uno tiene alrededor de 100.000 palabras recogidas.

¿Cómo puede haber más, pero mucho más, parámetros que palabras?

¿Qué es un ejemplo de un parámetro?

Un parámetro es un valor en una matriz.

Cada lado de la matriz es proporcial al tamaño del dominio.

---

timeanddate.com is awesome, but you can also convet time zones on the command line.

The syntax is weird, but it works.

```
TZ="Target time zone" \
date --date='TZ="Source time zone" source:time' \
--iso-8601=seconds
```

```
$ TZ="America/Los_Angeles" date --date='TZ="Europe/Madrid" 04:14' --iso-8601=seconds
2021-03-26T20:14:00-07:00
```

Use tzselect to interactively look up a time zone based on geography. Don't let
the messaging confuse you. It doesn't change the system time zone.

---

When you try to upload a CSV to a Cognito user pool without admin permissions, you get an opaque error.

> You don&apos;t have sufficient permission for this operation. Please contact your administrator.

This happens even if you have assume a role with the AmazonCognitoPowerUser policy.

It might be because of a missing iam:PassRole permission.

Unfortunately the best documentation I can find for this is a stack overflow answer.

https://stackoverflow.com/questions/59952459/cognito-create-import-job-insufficient-permission

https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_passrole.html

https://www.reddit.com/r/aws/comments/huey2g/cognito_csv_import_broken/

https://www.reddit.com/r/aws/comments/jbwkh8/aws_iampassrole_explained_with_pictures/

https://blog.rowanudell.com/iam-passrole-explained/


