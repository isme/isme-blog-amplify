# Choosing a credential scanner

## Research

Read about the state of the art in a November 2020 article from Geekflare.
https://geekflare.com/github-credentials-scanner/

Consider the command-based options (didn't even consider the service- or
web-based options).

https://github.com/dxa4481/truffleHog
last updated 19, 20, 24, 25 days ago and 1 year ago
5.5k stars
by RIT Computer Engineer Graduate Dylan Ayrey
installed using pip
preferred for frequent updates and easy installation

https://github.com/awslabs/git-secrets
updated 9 months ago and 2 years ago
8.7k stars
by AWS
preferred because of brand name recognition

https://github.com/ezekg/git-hound
last updated 5 months ago and then 4 years ago
269 stars
discarded for infrequent updates

https://github.com/UKHomeOffice/repo-security-scanner
updated 17 months ago, then 4 years ago
973 stars
discarded for infrequent updates

https://github.com/kootenpv/gittyleaks
last updated 6 years ago
394 stars
discarded for infrequent updates

https://github.com/feeltheajf/truffleHog3
last updated 18 days ago and 7 months ago
27 stars
forked from dxa4481/truffleHog
This branch is 188 commits ahead, 125 commits behind dxa4481:dev
installed using pip https://pypi.org/project/truffleHog3/
discarded for infrequent updates

## Testing

### Trufflehog

Simple to install.

```
$ pipx install truffleHog
  installed package trufflehog 2.2.1, Python 3.8.5
  These apps are now globally available
    - trufflehog
done! ✨ 🌟 ✨
```

Works great to identify AWS keys. Disable entropy mode to reduce noise from
logs containing.

```
$ trufflehog --entropy=False --regex file:///home/isme/Repos/dentsu-notes/
~~~~~~~~~~~~~~~~~~~~~
Reason: AWS API Key
Date: 2021-02-23 10:11:27
Hash: 55730e006fc71d558f7f359f062302dae6478723
Filepath: trackingsystem-terraform-hubert/RunTerraformForClientLocal - Copy
Branch: origin/master
Commit: Log work

AKIASIHVIBXKQRAJPJMT
~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~
Reason: AWS API Key
Date: 2021-01-29 20:35:23
Hash: 93629cd2cb90636269ea843c347c4f9b71b986aa
Filepath: creating-a-new-primary-account.md
Branch: origin/master
Commit: Log work

AKIA2XIKTVVVDTFQV7EO
~~~~~~~~~~~~~~~~~~~~~
```

The entropy detector does find the secret access keys, but it also finds git
commit hashes and other unique identifiers that are not dangerous to expose. It
also seems to print the entire diff output, which can be a lot of irrevant lines
for commits of log files. I'm not sure how to reduce the output without
disabling it entirely.

### git-secrets

It's difficult to install. I wrote a script for it.

TODO: test.

https://sweetcode.io/how-use-truffle-hog-git-secrets/

https://github.com/aws-samples/aws-security-workshops/
