Looking for something techy to read?

* [AJ ONeal](https://coolaj86.com/archive/)
* [Mark Brooker](https://brooker.co.za/blog/)
* [Aidan Steele](https://awsteele.com/)
* [Dave Jarvis](https://dave.autonoma.ca/)
* [Kyler Middleton](https://kymidd.medium.com/)'s "Let's Do DevOps" series presents complex DevOps projects and simple and approachable via plain lanugage and lots of pictures.
