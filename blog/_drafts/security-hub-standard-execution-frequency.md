---
layout: post
title: Security Hub Standard Execution Frequency
---

A client wants to know how much Security Hub will cost so we can budget for it.

We know the cost depends on the number of checks that are executed. How
frequently are they executed?

My client would like to do a daily check for all the controls in the security
standards.

It turns out we have no control over the execution frequency. In AWS Config the
rules managed by Security Hub show a warning.

> You cannot edit or delete these rules if you are subscribed to AWS services
> that these rules are linked to.

The frequency varies for each check.

Some are triggered periodically, such as every 12 hours, like the IAM root
access key check.

![](iam%20root%20access%20key%20check.png)

Others are triggered by configuration changes, like the S3 blacklisted actions
prohibited check.

![](s3%20bucket%20blacklisted%20actions%20prohibited%20check.png)


The Usage tab of the Security Hub Settings screen gives a summary of the pricing
model and an estimate of the based on the number of checks and the number of
ingested events.

![](security%20hub%20cost%20estimate.png)

So the best way to estimate the cost according to the execution frequency seems
to be to leave it running for a time (a week, a month) and later see the usage
summary.

Probably for this reason Security Hub offers a free tial.
