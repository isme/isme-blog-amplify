# Things I Learned in July 2022

I took advantage of a project break to continue studying for the AWS Solutions
Architect Professional certification (SAP-C01). I'm following Adrian Cantrill's
course.

I created a dynamic, highly-available, accelerated site-to-site VPN between an
AWS location and a simulated on-premises environment. I configured a transit
gateway, two customer gateways, and two on-premises routers running Strongswan
and FRRouting to set up an IPSec tunnel and BGP routing.

TODO: Client VPN

I mastered querying AWS Config programmatically. Now I can use it to generate
inventory reports across a whole organization based on any of the properties it
records. I worked around its lack of joining to get subsets of resources by
implementing a function that get a list of resources in chunks. Each chunk fits
in the query size limit. This is an order of magnitute faster than downloading
the whole list of resources and subsetting on the client side.
