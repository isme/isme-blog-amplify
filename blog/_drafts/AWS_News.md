AWS News

Check AWS Config costs month-on-month for envidence of price reduction. https://aws.amazon.com/about-aws/whats-new/2022/09/aws-config-price-reduction-58-percent-conformance-packs/

Subscribe to Security Hub announcements: https://docs.aws.amazon.com/securityhub/latest/userguide/securityhub-announcements.html

Build AWS Config rules using AWS CloudFormation Guard
https://aws.amazon.com/about-aws/whats-new/2022/08/build-aws-config-rules-cloudformation-guard/

Try CloudWatch Embedded Metrics format
https://aws.amazon.com/about-aws/whats-new/2019/11/amazon-cloudwatch-launches-embedded-metric-format/

AWS Config conformance packs now provide scores to help you track resource compliance
https://aws.amazon.com/about-aws/whats-new/2022/07/aws-config-conformance-packs-scores-track-resource-compliance/

AWS Backup adds legal hold capability for extended data retention beyond lifecycle policies
https://aws.amazon.com/about-aws/whats-new/2022/11/aws-backup-legal-hold-data-retention-beyond-lifecycle-policies/

AWS CloudTrail Lake now supports configuration items from AWS Config
https://aws.amazon.com/about-aws/whats-new/2022/11/aws-cloudtrail-lake-supports-configuration-items-aws-config/

Amazon CloudWatch launches cross-account observability across multiple AWS accounts
https://aws.amazon.com/about-aws/whats-new/2022/11/amazon-cloudwatch-cross-account-observability-multiple-aws-accounts/

Announcing delegated administrator for AWS Organizations
https://aws.amazon.com/about-aws/whats-new/2022/11/aws-organizations-delegated-administrator/

AWS Backup launches application-aware data protection for applications defined using AWS CloudFormation
https://aws.amazon.com/about-aws/whats-new/2022/11/aws-backup-application-aware-data-protection-applications-defined-cloudformation/

AWS Config rules now support proactive compliance
https://aws.amazon.com/about-aws/whats-new/2022/11/aws-config-rules-support-proactive-compliance/

AWS announces Amazon Inspector support for AWS Lambda functions
https://aws.amazon.com/about-aws/whats-new/2022/11/aws-amazon-inspector-support-aws-lambda-functions/

Amazon Macie introduces automated sensitive data discovery
https://aws.amazon.com/about-aws/whats-new/2022/11/amazon-macie-automated-sensitive-data-discovery/

Announcing data protection in Amazon CloudWatch Logs, helping you detect, and protect sensitive data-in-transit
https://aws.amazon.com/about-aws/whats-new/2022/11/data-protection-amazon-cloudwatch-logs-detect-protect-sensitive-data-in-transit/

Amazon EventBridge Pipes is now generally available
https://aws.amazon.com/about-aws/whats-new/2022/12/amazon-eventbridge-pipes-generally-available/

AWS Backup launches delegation of organization-wide backup administration
https://aws.amazon.com/about-aws/whats-new/2022/11/aws-backup-delegation-organization-wide-backup-administration/

Announcing comprehensive controls management with AWS Control Tower (Preview)
https://aws.amazon.com/about-aws/whats-new/2022/11/aws-control-tower-preview-comprehensive-controls-management/

AWS announces Amazon Verified Permissions (Preview)
https://aws.amazon.com/about-aws/whats-new/2022/11/amazon-verified-permissions-preview/

Amazon VPC Reachability Analyzer now supports network reachability analysis across accounts in an AWS Organization
https://aws.amazon.com/about-aws/whats-new/2022/11/amazon-vpc-reachability-analyzer-network-analysis-accounts-aws-organization/

Introducing account customization within AWS Control Tower
https://aws.amazon.com/about-aws/whats-new/2022/11/aws-control-tower-account-customization/

Announcing the preview of AWS Verified Access
https://aws.amazon.com/about-aws/whats-new/2022/11/aws-verified-access-preview/

Introducing Amazon Security Lake (Preview)
https://aws.amazon.com/about-aws/whats-new/2022/11/amazon-security-lake-preview/

AWS KMS launches External Key Store
https://aws.amazon.com/about-aws/whats-new/2022/11/aws-kms-external-key-store/

Amazon GuardDuty RDS Protection now in preview
https://aws.amazon.com/about-aws/whats-new/2022/11/amazon-guardduty-rds-protection-preview/

AWS Security Hub now integrates with AWS Control Tower (Preview)
https://aws.amazon.com/about-aws/whats-new/2022/12/aws-security-hub-integrates-aws-control-tower/

Target multiple resources type with wildcard configuration for AWS CloudFormation Hooks
https://aws.amazon.com/about-aws/whats-new/2022/12/target-multiple-resources-wildcard-configuration-aws-cloudformation-hooks/

AWS Config enables drift detection in Config Recorder
https://aws.amazon.com/about-aws/whats-new/2022/12/aws-config-drift-detection-config-recorder/

AWS Systems Manager Change Manager now displays AWS CloudTrail events associated with change requests
https://aws.amazon.com/about-aws/whats-new/2022/12/aws-systems-manager-change-manager-displays-aws-cloudtrail-change-requests/

Announcing the preview of AWS Verified Access
https://aws.amazon.com/about-aws/whats-new/2022/11/aws-verified-access-preview/

Advanced Notice: Amazon S3 will automatically enable S3 Block Public Access and disable access control lists for all new buckets starting in April 2023
https://aws.amazon.com/about-aws/whats-new/2022/12/amazon-s3-automatically-enable-block-public-access-disable-access-control-lists-buckets-april-2023/

Amazon CloudWatch launches Metrics Insights alarms
https://aws.amazon.com/about-aws/whats-new/2022/12/amazon-cloudwatch-metrics-insights-alarms/

Introducing concurrent account provisioning operations for AWS Control Tower
https://aws.amazon.com/about-aws/whats-new/2022/12/aws-control-tower-concurrent-account-provisioning-operations/

Announcing Open Data Maps for Amazon Location Service (Preview)
https://aws.amazon.com/about-aws/whats-new/2022/12/amazon-location-service-open-data-maps-preview/

AWS Trusted Advisor adds new fault tolerance checks
https://aws.amazon.com/about-aws/whats-new/2022/12/aws-trusted-advisor-fault-tolerance-checks/

AWS Cost Anomaly Detection now supports percentage-based thresholds
https://aws.amazon.com/about-aws/whats-new/2022/12/aws-cost-anomaly-detection-percentage-based-thresholds/

Amazon Route 53 now offers threat intelligence sourced from Recorded Future
https://aws.amazon.com/about-aws/whats-new/2022/12/amazon-route-53-threat-intelligence-recorded-future/

AWS IQ launches public profiles for companies
https://aws.amazon.com/about-aws/whats-new/2022/12/aws-iq-launches-public-profiles-companies/

AWS Storage Gateway introduces Terraform modules for Amazon S3 File Gateway
(This is interesting mostly because it's a reminder that AWS also maintains Terraform modules! See the aws-ia Github organization.)
https://aws.amazon.com/about-aws/whats-new/2022/12/aws-storage-gateway-terraform-modules-amazon-s3-file-gateway/

AWS announces Systems Manager Quick Setup for Resource Scheduler
Another way to run the Instance Scheduler solution.
https://aws.amazon.com/about-aws/whats-new/2022/12/aws-systems-manager-quick-setup-resource-scheduler/

https://www.youtube.com/watch?v=Dh52W_KcSO4

Amazon RDS announces integration with AWS Secrets Manager
"With this feature, RDS fully manages the master user password and stores it in AWS Secrets Manager whenever your RDS database instances are created, modified, or restored. The new feature supports the entire lifecycle maintenance for your RDS master user password including regular and automatic password rotations; removing the need for you to manage rotations using custom Lambda functions."
https://aws.amazon.com/about-aws/whats-new/2022/12/amazon-rds-integration-aws-secrets-manager/

AWS Organizations console adds support to centrally manage region opt-in settings on AWS accounts
https://aws.amazon.com/about-aws/whats-new/2022/12/aws-organizations-console-support-manage-region-opt-in-settings-accounts/

AWS Security Hub launches 9 new security best practice controls
https://aws.amazon.com/about-aws/whats-new/2022/12/aws-security-hub-new-security-best-practice-controls/

Specify validation checks for CommaDelimitedList parameter with AWS CloudFormation
https://aws.amazon.com/about-aws/whats-new/2022/12/specify-validation-checks-commadelimitedlist-parameter-aws-cloudformation/

AWS Config now supports 13 new resource types
https://aws.amazon.com/about-aws/whats-new/2022/12/aws-config-supports-new-resource-types/

Amazon CloudFront now supports the removal of response headers
https://aws.amazon.com/about-aws/whats-new/2023/01/amazon-cloudfront-supports-removal-response-headers/

Amazon CloudWatch Logs removes Log Stream transaction quota and SequenceToken requirement
https://aws.amazon.com/about-aws/whats-new/2023/01/amazon-cloudwatch-logs-log-stream-transaction-quota-sequencetoken-requirement/

Amazon S3 now automatically encrypts all new objects
https://aws.amazon.com/about-aws/whats-new/2023/01/amazon-s3-automatically-encrypts-new-objects/

AWS CloudFormation enhances Fn::FindInMap language extension to support default values and additional intrinsic functions
https://aws.amazon.com/about-aws/whats-new/2023/01/aws-cloudformation-fnfindinmap-language-extension-default-values-additional-intrinsic-functions/

AWS announces changes to AWS Billing, Cost Management, and Account consoles permissions
https://aws.amazon.com/about-aws/whats-new/2023/01/aws-changes-billing-cost-management-account-consoles-permissions/

AWS Config supports 22 new resource types
https://aws.amazon.com/about-aws/whats-new/2023/01/aws-config-22-new-resource-types/

AWS Resource Groups now emits lifecycle events
https://aws.amazon.com/about-aws/whats-new/2023/01/aws-resource-groups-emits-lifecycle-events/

AWS Clean Rooms is now available in preview
https://aws.amazon.com/about-aws/whats-new/2023/01/aws-clean-rooms-now-available-preview/

Amazon CloudWatch announces enhanced error visibility for Embedded Metric Format (EMF)
https://aws.amazon.com/about-aws/whats-new/2023/01/amazon-cloudwatch-enhanced-error-visibility-embedded-metric-format-emf/

EC2 network performance metrics add support for ConnTrack Utilization metric
https://aws.amazon.com/about-aws/whats-new/2023/01/ec2-network-performance-metrics-support-conntrack-utilization-metric/

Validate AWS Serverless Application Model (SAM) templates with CloudFormation Linter to speed up development
https://aws.amazon.com/about-aws/whats-new/2023/01/validate-aws-serverless-application-model-templates-cloudformation-linter/

Amazon CloudWatch launches cross-account Metric Streams
https://aws.amazon.com/about-aws/whats-new/2023/01/amazon-cloudwatch-cross-account-metric-streams/

Announcing runtime management controls for AWS Lambda
https://aws.amazon.com/about-aws/whats-new/2023/01/runtime-management-controls-aws-lambda/

Amazon EC2 Launch Templates now support AWS Systems Manager parameters for AMIs
https://aws.amazon.com/about-aws/whats-new/2023/01/amazon-ec2-launch-templates-aws-systems-manager-parameters-amis/

Announcing comprehensive controls management with AWS Control Tower
https://aws.amazon.com/about-aws/whats-new/2023/01/controls-management-aws-control-tower/

Amazon CloudWatch now simplifies metric extraction from structured logs
https://aws.amazon.com/about-aws/whats-new/2023/01/amazon-cloudwatch-metric-extraction-structured-logs/

AWS CloudTrail Lake now supports ingestion of activity events from non-AWS sources
https://aws.amazon.com/about-aws/whats-new/2023/01/aws-cloudtrail-lake-ingestion-activity-events-non-aws-sources/

AWS CloudFormation StackSets gives quick access to list of Regions for stack instances of a stack set
https://aws.amazon.com/about-aws/whats-new/2023/02/aws-cloudformation-stacksets-access-list-regions-stack-instances-stack-set/
(New `.Regions` property in response.)

AWS CloudFormation announces spotlight for latest news on CloudFormation features, blogs, and workshops
https://aws.amazon.com/about-aws/whats-new/2023/02/aws-cloudformation-news-features-blogs-workshops/

AWS Config now supports 20 new resource types
https://aws.amazon.com/about-aws/whats-new/2023/02/aws-config-20-resource-types/

Programmatically manage enabled and disabled opt-in AWS Regions on AWS accounts
https://aws.amazon.com/about-aws/whats-new/2023/02/manage-enabled-disabled-opt-in-aws-regions-accounts/
(The CLI doesn't automatically paginate `aws account list-regions`.)

AWS WAF Captcha adds support for ten additional languages
https://aws.amazon.com/about-aws/whats-new/2023/02/aws-waf-captcha-ten-additional-languages/
(I didn't know WAF supported captcha!)

Amazon Cognito identity pool data events are now available in AWS CloudTrail
https://aws.amazon.com/about-aws/whats-new/2023/02/amazon-cognito-identity-pool-data-events-aws-cloudtrail/

Request tracing for customizations now available for AWS Control Tower Account Factory for Terraform
https://aws.amazon.com/about-aws/whats-new/2023/02/tracing-customizations-aws-control-tower-account-factory-terraform/
(More official support for Terraform-based solutions. Is this the official way to programatically control Control Tower?)

Amazon CloudWatch announces increased quotas for Logs Insights
https://aws.amazon.com/about-aws/whats-new/2023/02/amazon-cloudwatch-increased-quotas-logs-insights/
("Amazon CloudWatch Logs Insights has increased the log group quota from 20 to 50, increased query timeouts from 15 minutes to 60 minutes, and increased query concurrency quota from 20 to 30. By increasing the queried log groups’ quotas from 20 to 50, customers can now select up to 50 log groups in a single query. With the increased query timeout from 15 mins to 60 mins, customers can successfully execute long-running queries. With increased concurrency from 20 to 30, customers can now run 30 queries parallel.")

AWS Step Functions adds integration for 35 services including EMR Serverless
https://aws.amazon.com/about-aws/whats-new/2023/02/aws-step-functions-integration-35-services-emr-serverless/

Announcing the ability to enable AWS Systems Manager by default across all EC2 instances in an account
https://aws.amazon.com/about-aws/whats-new/2023/02/enable-aws-systems-manager-default-all-ec2-instances-account/
("DHMC also simplifies the experience of managing access to EC2 instances by attaching permissions at the account level, and removing the requirement to alter existing instance profile roles to enable Systems Manager. You can begin utilizing the benefits of DHMC in just a few clicks from the Fleet Manager console.") This seems like a really useful feature.

Announcing AWS Telco Network Builder
https://aws.amazon.com/about-aws/whats-new/2023/02/aws-telco-network-builder/
(An automatic builder for multi-region networks? A wrapper around EKS implementing teleco protocols and concepts.)

Amazon CloudWatch Internet Monitor is now generally available
https://aws.amazon.com/about-aws/whats-new/2023/02/amazon-cloudwatch-internet-monitor-generally-available/

Introducing Amazon Lightsail for Research
https://aws.amazon.com/about-aws/whats-new/2023/03/amazon-lightsail-research/
("Amazon Lightsail now offers Amazon Lightsail for Research, a new offering that makes it simple for you to accelerate your research using the power of the cloud. Lightsail for Research provides access to analytical applications such as Scilab, RStudio, and Jupyter running on powerful virtual computers in just a few clicks.")
(A really easy way for data scientists to get started in AWS?)

AWS Control Tower announces a progress tracker for landing zone setup and upgrades
https://aws.amazon.com/about-aws/whats-new/2023/03/aws-control-tower-progress-tracker-landing-zone-setup-upgrades/


IAM Roles for Amazon EC2 now provide Credential Control Properties
https://aws.amazon.com/about-aws/whats-new/2023/03/iam-roles-amazon-ec2-credential-control-properties/
https://aws.amazon.com/blogs/security/how-to-use-policies-to-restrict-where-ec2-instance-credentials-can-be-used-from/


AWS Security Hub launches support for NIST SP 800-53 Rev. 5
https://aws.amazon.com/about-aws/whats-new/2023/03/aws-security-hub-support-nist-sp-800-53-rev-5/
A new standard. Now there are 4.

Delegated administrator for AWS Organizations launches in the AWS GovCloud (US) Regions
https://aws.amazon.com/about-aws/whats-new/2023/03/delegated-administrator-aws-organizations-aws-govcloud-us-regions/
Not as general as for the commercial region. Only supports tag policies and backup policies.

Announcing Open Data Maps for Amazon Location Service
https://aws.amazon.com/about-aws/whats-new/2023/03/amazon-location-service-open-data-maps/
Now OSM data is out of preview! "Developers can rely on the availability, low latency, security, and reliability of Amazon Location Open Data Maps, without the need to set up and operate specialized OSM tools. In addition, developers no longer need to be concerned with the freshness of their location data as Amazon Location refreshes the data on a regular basis."

Amazon Virtual Private Cloud (VPC) Prefix Lists now available in two additional regions
https://aws.amazon.com/about-aws/whats-new/2023/03/amazon-vpc-prefix-lists-two-regions/
Europe (Zurich) and Middle East (UAE). I don't normally record regional rollouts, but Zurich is part of my current project.

AWS License Manager Linux subscriptions expands region and administrator support
https://aws.amazon.com/about-aws/whats-new/2023/03/aws-license-manager-linux-subscriptions-expands-region-administrator-support/
One less reason to access the management account.

Amazon Route 53 Resolver endpoints for hybrid cloud are now available in the Europe (Zurich) Region
https://aws.amazon.com/about-aws/whats-new/2023/03/amazon-route-53-resolver-endpoints-hybrid-cloud-zurich/
Something new in Zurich.

AWS Data Exchange for Amazon S3 is now generally available
https://aws.amazon.com/about-aws/whats-new/2023/03/aws-data-exchange-amazon-s3/
Can you use this to share CloudTrail archives with customer accounts?

Amazon S3 simplifies private connectivity from on-premises networks
https://aws.amazon.com/about-aws/whats-new/2023/03/amazon-s3-private-connectivity-on-premises-networks/
Makes S3 cross-region cheaper? Not sure about this, but it looks useful.

Amazon OpenSearch Service introduces security analytics
https://aws.amazon.com/about-aws/whats-new/2023/03/amazon-opensearch-service-security-analytics/
"Security analytics is built on open source OpenSearch and comes pre-packaged with over 2200 open source Sigma security rules."

Application Auto Scaling now supports Metric Math for Target Tracking policies
https://aws.amazon.com/about-aws/whats-new/2023/03/application-auto-scaling-metric-math-target-tracking-policies/
This reduces the need to pay for a custom metrics for scaling.

Announcing Amazon Linux 2023
https://aws.amazon.com/about-aws/whats-new/2023/03/amazon-linux-2023/
Finally! Amazon Linux 2 had really old versions of glibc.

Amazon GuardDuty RDS Protection for Amazon Aurora is now generally available
https://aws.amazon.com/about-aws/whats-new/2023/03/amazon-guardduty-rds-protection-aurora-generally-available/

Amazon Connect launches support for multiple SAML 2.0 identity providers
https://aws.amazon.com/about-aws/whats-new/2023/03/amazon-connect-multiple-saml-2-0-identity-providers/
To support migrations. What about Identity Center?

Amazon VPC Reachability Analyzer now supports 3 additional AWS networking services
https://aws.amazon.com/about-aws/whats-new/2023/03/amazon-connect-multiple-saml-2-0-identity-providers/
I like reacability analyzer because it helps me debug networking.

AWS CloudFormation language extensions transform is now available in 5 additional AWS Regions
https://aws.amazon.com/about-aws/whats-new/2023/03/aws-cloudformation-language-extensions-transform-5-regions/
Quality of life improvements via more intrinsic functions.

AWS Security Hub is now available in three additional AWS Regions
https://aws.amazon.com/about-aws/whats-new/2023/03/aws-security-hub-available-additional-aws-regions/
Hyderabad, Spain, Zurich. I care about some of those regions now.

Allow Listing tool for testing new Billing, Cost Management and Account console permissions
https://aws.amazon.com/about-aws/whats-new/2023/03/allow-listing-tool-testing-billing-cost-management-account-console-permissions/
Fine-grained permissions for billing. cost management, and account services.

AWS Systems Manager Incident Manager announces the launch of on-call schedules
https://aws.amazon.com/about-aws/whats-new/2023/03/aws-systems-manager-on-call-schedules/
Does this mean I don't need to use OpsGenie any more?

Amazon Athena adds minimum encryption to enhance query result security
https://aws.amazon.com/about-aws/whats-new/2023/03/amazon-athena-minimum-encryption-query-result-security/
"When you query data, sensitive information may be included in the result. To reduce the impact of unauthorized access by an untrusted third-party, it is recommended that you encrypt your query results. Today, you can set a default encryption level for queries within a workgroup. However, users can, if permitted, override the default and use a different encryption level for individual queries. With this release, you can now ensure all query results are encrypted with a desired minimum level of encryption and choose one of several methods of varying strength to safeguard your data."

Amazon GuardDuty simplifies enforcement of threat detection across all accounts in an Organization
https://aws.amazon.com/about-aws/whats-new/2023/03/amazon-guardduty-enforcement-threat-detection-organization/
"Now with a few steps in the GuardDuty console, or one API call, delegated administrators can enforce GuardDuty threat detection coverage for their organization by automatically applying the service to all existing and new accounts, as well as automatically identifying and remediating potential coverage drift."

AWS Service Catalog is now available in four additional AWS Regions
https://aws.amazon.com/about-aws/whats-new/2023/03/aws-service-catalog-four-regions/
"Europe (Zurich), Europe (Spain), Asia Pacific (Hyderabad) and Asia Pacific (Melbourne). "

AWS WAF is now available in the Zurich, Spain, Hyderabad, and Melbourne regions
https://aws.amazon.com/about-aws/whats-new/2023/03/aws-waf-additional-regions/

AWS re:Post now includes AWS Knowledge Center articles
https://aws.amazon.com/about-aws/whats-new/2023/03/aws-re-post-includes-knowledge-center-articles/
No more premium support articles!

Amazon GuardDuty now monitors runtime activity from containers running on Amazon EKS
https://aws.amazon.com/about-aws/whats-new/2023/03/amazon-guardduty-monitors-runtime-activity-containers-eks/

AWS Well-Architected Tool Announces Consolidated Report and Enhanced Search functionality
https://aws.amazon.com/about-aws/whats-new/2023/03/aws-well-architected-tool-consolidated-report-enhanced-search/

AWS Compute Optimizer now supports EC2 instances with non-consecutive utilization data
https://aws.amazon.com/about-aws/whats-new/2023/03/aws-compute-optimizer-ec2-instances-non-consecutive-utilization-data/
Now you can stop an instance and still get rightsizing recommenations. Good for office hours instances.

AWS Site-to-Site VPN adds support for better visibility and control of VPN tunnel maintenance updates
https://aws.amazon.com/about-aws/whats-new/2023/03/aws-site-vpn-visibility-control-tunnel-maintenance-updates/
Now you can choose when and if your tunnel goes down for updates!

---

Working backwards:

AWS Shield Advanced is now available in four additional AWS Regions
https://aws.amazon.com/about-aws/whats-new/2023/05/aws-shield-advanced-four-regions/
"AWS Europe (Zurich), Europe (Spain), Australia (Melbourne), and Asia Pacific (Hyderabad) Regions"

AWS CloudFormation StackSets is now available in 3 additional AWS Regions
https://aws.amazon.com/about-aws/whats-new/2023/05/aws-cloudformation-stacksets-additional-regions/
"Asia Pacific (Melbourne), Europe (Spain), and Europe (Zurich)"
Until now I had just assumed that stack sets were available in all regions because CloudFormation was. It turns out that Zurich got support only today!

Read up to here:

Announcing the ACK Controllers for Amazon EventBridge and Pipes
03/30/2023
page 28

https://aws.amazon.com/new/?whats-new-content-all.sort-by=item.additionalFields.postDateTime&whats-new-content-all.sort-order=desc&awsf.whats-new-analytics=*all&awsf.whats-new-app-integration=*all&awsf.whats-new-arvr=*all&awsf.whats-new-blockchain=*all&awsf.whats-new-business-applications=*all&awsf.whats-new-cloud-financial-management=*all&awsf.whats-new-compute=*all&awsf.whats-new-containers=*all&awsf.whats-new-customer-enablement=*all&awsf.whats-new-customer%20engagement=*all&awsf.whats-new-database=*all&awsf.whats-new-developer-tools=*all&awsf.whats-new-end-user-computing=*all&awsf.whats-new-mobile=*all&awsf.whats-new-gametech=*all&awsf.whats-new-iot=*all&awsf.whats-new-machine-learning=*all&awsf.whats-new-management-governance=*all&awsf.whats-new-media-services=*all&awsf.whats-new-migration-transfer=*all&awsf.whats-new-networking-content-delivery=*all&awsf.whats-new-quantum-tech=*all&awsf.whats-new-robotics=*all&awsf.whats-new-satellite=*all&awsf.whats-new-security-id-compliance=*all&awsf.whats-new-serverless=*all&awsf.whats-new-storage=*all&awsf.whats-new-categories=*all&awsm.page-whats-new-content-all=28