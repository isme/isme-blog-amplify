[pipx](https://pypi.org/project/pipx/) is how you install applications that are written in Python. You don't actually care that they are written in Python, you just need the executable to be available on your system PATH. Each application gets its own virtual environment to avoid version conflicts.

[pew](https://github.com/berdario/pew) is the quickest way to create a temporary virtual environment for one-off tests. It appears unmaintained since 2019, but it still works.

```
$ pew mktmpenv
created virtual environment CPython3.8.5.final.0-64 in 107ms
  creator CPython3Posix(dest=/home/isme/.local/share/virtualenvs/78a71b086bcb028, clear=False, no_vcs_ignore=False, global=False)
  seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=/home/isme/.local/share/virtualenv)
    added seed packages: pip==21.1.2, setuptools==57.0.0, wheel==0.36.2
  activators BashActivator,CShellActivator,FishActivator,PowerShellActivator,PythonActivator,XonshActivator
This is a temporary environment. It will be deleted when you exit
Launching subshell in virtual environment. Type 'exit' or 'Ctrl+D' to return.
$ pip freeze
$ 
```

[py-spy](https://github.com/benfred/py-spy) is a sampling profiler for Python programs. It lets you visualize what your Python program is spending time on without restarting the program or modifying the code in any way.

[ClusterFuzzLite](https://google.github.io/clusterfuzzlite/) is a continuous fuzzing solution that runs as part of Continuous Integration (CI) workflows to find vulnerabilities faster than ever before. With just a few lines of code, GitHub users can integrate ClusterFuzzLite into their workflow and fuzz pull requests to catch bugs before they are committed. It looks like a fuzzer that is actually usable.

[sh](https://github.com/amoffat/sh) is a full-fledged subprocess replacement for Python 2, Python 3, PyPy and PyPy3 that allows you to call any program as if it were a function. sh is not a collection of system commands implemented in Python. sh relies on various Unix system calls and only works on Unix-like operating systems - Linux, macOS, BSDs etc. Specifically, Windows is not supported.

[HashFS](https://github.com/dgilland/hashfs) is a content-addressable file management system. What does that mean? Simply, that HashFS manages a directory where files are saved based on the file's hash.

[Birdseye](https://github.com/alexmojaki/birdseye) is a graphical Python debugger which lets you easily view the values of all evaluated expressions.

[Sorcery](https://github.com/alexmojaki/sorcery) lets you use and write callables called 'spells' that know where they're being called from and can use that information to do otherwise impossible things. Not for production code.

[Python Tutor](https://pythontutor.com/) executes Python cade that you give it line by line and draws the result data structures. Great for understanding how variables, names, assignment and mutations work. Thanks to Ned Batchelder for sharing in his talk [Facts and Myths about Python Names nd Values](https://www.youtube.com/watch?v=_AEJHKGk9ns). Try it with his example

```python
badboard = [[0] * 8] * 8
goodboard = [[0] * 8 for _ in range(8)]
```

[pygrametl](http://chrthomsen.github.io/pygrametl/doc/index.html) is a package for creating Extract-Transform-Load (ETL) programs in Python. The package contains several classes for filling fact tables and dimensions (including snowflaked and slowly changing dimensions), classes for extracting data from different sources, classes for optionally defining an ETL flow using steps, classes for parallelizing an ETL flow, classes for testing an ETL flow, and convenient functions for often-needed ETL functionality.)

[funcy](https://github.com/Suor/funcy) A collection of fancy functional tools
focused on practicality. Inspired by clojure, underscore and the author's own
abstractions.

[blacken-docs](https://github.com/asottile/blacken-docs) runs black on python
code blocks in documentation files.

[pyinstrument](https://github.com/joerick/pyinstrument) is a call stack profiler
for Python. It shows you why your code is slow!

[pipdeptree](https://github.com/naiquevin/pipdeptree) is a command line utility
for displaying the installed python packages in form of a dependency tree.

[AWS Lambda Typing](https://github.com/MousaZeidBaker/aws-lambda-typing) provides type hints for AWS Lambda event, context, and response objects.

[AWS Lambda Powertools for Python](https://github.com/awslabs/aws-lambda-powertools-python) is a suite of utilities for AWS Lambda Functions that makes tracing with AWS X-Ray, structured logging and creating custom metrics asynchronously easier. Include typing for Lambda context.

[Parsl](https://github.com/Parsl/parsl) is a parallel programming library for
Python. Parsl augments Python with simple, scalable, and flexible constructs for
encoding parallelism. Developers annotate Python functions to specify
opportunities for concurrent execution. These annotated functions, called apps,
may represent pure Python functions or calls to external applications, whether
sequential, multicore (e.g., CPU, GPU, accelerator), or multi-node MPI. Parsl
further allows these calls to these apps, called tasks, to be connected by
shared input/output data (e.g., Python objects or files) via which Parsl can
construct a dynamic dependency graph of tasks.

[python-renameat2](https://github.com/jordemort/python-renameat2) is a wrapper (using CFFI) around Linux's renameat2 system call. With renameat2, you can atomically swap two files, choose if existing files are replaced, and create "whiteout" files for overlay filesystems.

[Strictly Typed Pandas](https://github.com/nanne-aben/strictly_typed_pandas): static type checking of pandas DataFrames. I love Pandas! But in production code I’m always a bit wary when I see `foo(df: pd.DataFrame) -> pd.DataFrame` Because… How do I know which columns are supposed to be in df?

[bigjson](https://github.com/henu/bigjson) reads JSON files of any size.
