---
layout: default
---

Hi, I'm Iain.

I am a certified AWS solutions architect and security specialist. I have 12
years' experience as an IT professional and 7 years' building solutions in the
AWS cloud. I specialize in architecting, implementing, and operating security
infrastructure at an enterprise scale.

See a recent [CV in PDF format]({% link CV_for_Iain_Samuel_McLean_Elder.pdf %}).

<figure>
  <figcaption style="text-align:center;">
    My AWS certification badges. Click through to verify me on Credly.
  </figcaption>
  <div class="badgerow" style="display:flex;">
    <div class="badgecol" style="flex:33%;">
      <a href="https://www.credly.com/badges/bd745d05-8819-4689-9d2b-1edf45574423">
        <figure>
          <img
            src="/assets/saa_badge.png"
            alt="AWS certification badge: Solutions Architect Associate"
          >
          <figcaption style="text-align:center;">
            Solutions Architect Associate since 2018
          </figcaption>
        </figure>
      </a>
    </div>
    <div class="badgecol" style="flex:33%;">
      <a href="https://www.credly.com/badges/5f68766b-630a-4b4d-a99c-0478caa23835">
        <figure>
          <img
            src="/assets/scs_badge.png"
            alt="AWS certification badge: Security Specialty"
          >
          <figcaption style="text-align:center;">
            Security Specialty since 2020
          </figcaption>
        </figure>
      </a>
    </div>
    <div class="badgecol" style="flex:33%;">
      <a href="https://www.credly.com/badges/b6054f29-6e34-4d7e-ab57-397d47f78c78">
        <figure>
          <img
            src="/assets/sap_badge.png"
            alt="AWS certification badge: Solutions Architect Professional"
          >
          <figcaption style="text-align:center;">
            Solutions Architect Professional since 2023
          </figcaption>
        </figure>
      </a>
    </div>
  </div>
</figure>

You can find me here:

* [Linkedin](https://www.linkedin.com/in/isme-devops/)
* [Upwork](https://www.upwork.com/freelancers/~014e293d115c081e2a)
* [Github](https://github.com/iainelder)
* [Bitbucket](https://bitbucket.org/isme/)
* [Atlassian Community](https://community.atlassian.com/t5/user/viewprofilepage/user-id/3187709)
* [AWS Forums](https://forums.aws.amazon.com/profile.jspa?userID=480983)
* [CloudFlare Community](https://community.cloudflare.com/u/isme/activity)
* [Credly](https://www.youracclaim.com/users/iain-samuel-mclean-elder/badges)
* [1Password Supoprt Community](https://1password.community/profile/discussions/isme_es)
* [E-mail](mailto:iain@isme.es)
