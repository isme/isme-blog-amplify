---
layout: default
title: Reading
---

I'm so busy doing AWS that I don't find much time to write about it publicly.

These people do, somehow, and I immensely appreciate their sharing their
experience.

This list will grow as I collect more articles.

* [A Guide to S3 Logging](https://ramimac.me/s3-logging) by Rami McCarthy (2023-03-29). A review of S3 logging techniques. Via Zoph.
* [Exploring Amazon VPC Lattice](https://onecloudplease.com/blog/exploring-amazon-vpc-lattice) by Ian McKay (2023-04-01). Intro to VPC lattice. Via Zoph.
* [A Dismal Guide to AWS Billing](https://carlos.bueno.org/2023/03/aws-dismal-guide.html) by Carlos Bueno (2023-03). This is like a missing manual for the AWS Cost and Usage Report (CUR). In principal it's a manual for [Cloudstats](https://github.com/aristus/cloudstats). Via Corey Quinn.
* [Cheating is all you need](https://about.sourcegraph.com/blog/cheating-is-all-you-need) by Steve Yegge (2023-03-23). This changed my mind about large language models (LLMs). Via Corey Quinn.
* https://tomharrisonjr.com/uuid-or-guid-as-primary-keys-be-careful-7b2aa3dcb439
* [Consistent Python environments with Poetry and pre-commit hooks ](https://objectpartners.com/2020/06/15/consistent-python-environments-with-poetry-and-pre-commit-hooks/) by Kenneth J Pronovici. 2020-06-15. I finally discovered the power of pre-commit hooks thanks to this article.
* [Is Your Ansible Package Configuration Secure?](https://blog.includesecurity.com/2021/06/hack-series-is-your-ansible-package-configuration-secure/) by Laurence Tennant. 2021-06-02. (Using strace to track which syscalls a process makes so that they may be filtered)
* [How to Handle Secrets on the Command Line](https://smallstep.com/blog/command-line-secrets/) by Carl Tashian. 2021-03-12.
* https://securingthe.cloud/aws/protecting-amazon-s3-data-from-ransomware/
* https://summitroute.com/blog/2021/08/03/S3_backups_and_other_strategies_for_ensuring_data_durability_through_ransomware_attacks/
* https://summitroute.com/blog/2021/08/05/lightsail_object_storage_concerns-part_1/
* [The last S3 security document that we’ll ever need, and how to use it](https://trustoncloud.com/the-last-s3-security-document-that-well-ever-need/) by Jonathan Rault. (2021-08-19)
*
  [Terraform Landing Zones for Self-Service Multi-AWS at Eventbrite](https://www.hashicorp.com/resources/terraform-landing-zones-for-self-service-multi-aws-at-eventbrite)
  [The Map of Cybersecurity Domains (version2.0)](https://www.linkedin.com/pulse/map-cybersecurity-domains-version-20-henry-jiang-ciso-cissp/
  ) by Henry Jiang (2021-03)
* [In defense of blub studies](https://www.benkuhn.net/blub/) by Ben Kuhn (2020-12)
* [Building a secure CI/CD pipeline for Terraform Infrastructure as Code](https://tech.ovoenergy.com/building-a-secure-ci-cd-pipeline-for-terraform-infrastructure-as-code/) by Chongyang Shi (2021-02-10)
* [ALB INGRESS Controller CrashLoopBackOffs in AWS EKS on FARGATE](https://www.lotharschulz.info/2020/01/29/alb-ingress-controller-crashloopbackoffs-in-aws-eks-on-fargate/?unapproved=19193&moderation-hash=c554a993b5b6523604de8c9dd13e581d#comment-19193)
* [Cloud Security Tabletop Exercises](https://levelup.gitconnected.com/cloud-security-table-top-exercises-629d353c268e) by Matt Fuller (2021-01-31)
* [Defining your consultancy niche part 2](https://www.lastweekinaws.com/podcast/screaming-in-the-cloud/defining-your-consultancy-niche-part-2-with-scott-piper/) by Corey Quinn and Scott Piper (2021-01-21)
* [How to Enable Logging on Every AWS Service in Existence (Circa 2021)](https://matthewdf10.medium.com/how-to-enable-logging-on-every-aws-service-in-existence-circa-2021-5b9105b87c9) by Matt Fuller (2021-01-4)
* [AWS Security Maturity Roadmap](https://summitroute.com/blog/2021/01/12/2021_aws_security_maturity_roadmap_2021/) by Scott Piper (2021-01-12)
* [So you inherited an AWS account](https://medium.com/swlh/so-you-inherited-an-aws-account-e5fe6550607d) by Matt Fuller (2020-04-28)
* [Conducting a Cloud Assessment](https://www.chrisfarris.com/post/cloud-assessment/) by Chris Farris (2020-06-14)
* [A Better Way to SSH in AWS](https://nullsweep.com/a-better-way-to-ssh-in-aws/) by Charlie Bellmer (2020-02-24)
* [SecDevOps: a five-minute explanation (esp)](https://www.elladodelmal.com/2019/02/secdevops-una-explicacion-en-cinco.html) by Chema Alonso (2019-02-11)
* [Secure access to 100 AWS accounts](https://segment.com/blog/secure-access-to-100-aws-accounts/) by Evan Johnson (2018-03-06)

Summary of [So you inherited an AWS account](https://medium.com/swlh/so-you-inherited-an-aws-account-e5fe6550607d):

 1. Get stable access
 2. Stop using the root user
 3. Update billing information
 4. Enable CloudTrail logging and monitoring
 5. Clean up IAM entities
 6. Locate exposed services
 7. Lock down your domains
 8. Find expiring certificates
 9. Untangle the web of services
10. Monitor and migrate
