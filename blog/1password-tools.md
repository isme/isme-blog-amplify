# 1Password Tools

[op-env](https://github.com/apiology/op_env) allows you to use 1Password entries
as environment variable-style secrets. This makes it easier to rotate secrets in
places where the source of truth is 1Password.

[pyonepassword](https://github.com/zcutlip/pyonepassword) is a Python API to
sign into and query a 1Password account using the op command.
