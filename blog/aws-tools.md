---
layout: default
title: AWS Tools
---

Here is a list of tools that make life easier when working with AWS.

It's forever a work in progress.

Unless otherwise stated, the tool works with AWS directly in some way. Some of
the tools are for supporting services such as external identity providers or
continuous integration services.

# Things I use and recommend

* [EC2Instances.info](https://instances.vantage.sh/), now hosted by Vantage, is
  still the easiest way to compare instance specs and prices. AWS has so many
  instance types, and one of the more esoteric ones might be cheaper than the
  general purpose ones for your use case. This tool makes it easy to discover,
  for example, that the cheapest instance with 16GB of RAM is the x2gd.medium.
  That's almost half the hourly on-demand price of the t3.xlarge that I would
  probably have chosen out of convenience if I didn't have this tool.
* [AWS CLI version 2][aws-cli-v2]. I call this out separately from V1 because
  it's unfortunately a different installation experience and more difficult to
  keep updated. There is an
  [ongoing debate on the Github project][aws-cli-debate] about how to resolve
  this. It's worth installing it just for the built-in SSO login support.
* [aws-gate](https://github.com/xen0l/aws-gate/) simplifies the use of Session
  Manager to use SSH to connect to resource in your VPC without opening any
  public access to your network. All the access is proxiewd through AWS APIs and
  controlled with IAM permissions. I added the `aws-gate ssh -L` option to make
  it easy to establish a [tunnel to other VPC resources with zero client-side
  configuration][aws-gate-pr].
* [saml2aws](https://github.com/Versent/saml2aws) automates the single-sign-on
  process for various third-party SSO providers. I can vouch for its utility in
  integrating JumpCloud. I improved the JumpCloud experience by making it [fail
  correctly when a 401 response does not prompt for MFA][saml2aws-pr].
* [AWS SAM CLI](https://github.com/aws/aws-sam-cli) is the best way to deploy
  CloudFormation stacks, even if you don't use any of the SAM syntax. The
  "deploy" command is used instead of the standard CLI's create-stack and
  update-stack commands and provides a synchronous experience. The command
  paints status updates on your resources until the final state of the stack is
  known. You no longer need to check the CloudFormation console or poll the
  stack status to know whether your stack deployed properly.
* [jccli](https://github.com/Sage-Bionetworks/jccli). A command line client used
  to administrate the hosted JumpCloud identity provider service. If you need to
  list users and groups, it might be faster to use this than clicking through
  the JumpCloud console.
* [Pipelines](https://github.com/ktomk/pipelines). A local pipeline runner for
  Bitbucket pipelines. Need to test the setup of CloudFormation stack from
  scratch in a pipeline? That can consume several of your precious build
  minutes. Save them by running the pipeline locally before committing wasteful
  errors.
* [clustergit](https://github.com/mnagel/clustergit) is great for making sure
  all your git repos are synced to the remotes. You just run clustergit in the
  parent folder and you get a status report for each repo to show you what you
  still have to commit. Then it can push everything up in parallel.
* [crudini](https://github.com/pixelb/crudini) - AWS CLI config files are
  written in INI file format, so you can use crudini to script changes to them
  or actions based on them.
* [awsls](https://github.com/jckuester/awsls) is a simpler way to list the
  resources of a certain type in an AWS account. Now you don't have to look up
  the API call for listing for each resource type, because this tool knows how
  to do it for you.
* [AWS Config Rules Repository](https://github.com/awslabs/aws-config-rules) are
  the conformance pack templates that you can see in the AWS Config console. You
  can't deploy to a an organization using the console. You need to use the CLI.
  And so you should clone this repo and pass the local path of your chosen
  conformance pack template to the `put-organization-conformance-pack` API.
* [AWS Service Matrix](https://services.adlinga.com/services) shows which
  services are available in which regions in a relatively easy to read format.
  Surprisingly few services are available everywhere. "It is worth noting that
  while a service may be supported in a particular region, this does not mean
  all features of that service will be availalbe. You should always check with
  the relevant product pages to determine the finer details." For example, the
  StackSets feature of CloudFormation is not broken out.
*
  [MAMIP (Monitor AWS Managed IAM Policies)](https://github.com/z0ph/aws_managed_policies)
  automates the retrieval of new AWS Managed IAM Policies make it easier to
  monitor and get alerted when changes occur using "Watch" feature of Github or
  Twitter Account.
*
  [AWS Config Resource Schema](https://github.com/awslabs/aws-config-resource-schema)
  property files define the properties and types of the AWS Config resource
  configuration items (CIs) that are searchable using the SelectResources API.
  You'll need to consult this to use AWS Config's advanced SQL queries. *
* [Search Public Buckets](https://buckets.grayhatwarfare.com/) a free tool that
  lists open s3 buckets and helps you search for interesting files. Don't be on
  this list.
* [s5cmd](https://github.com/peak/s5cmd) is a very fast S3 and local filesystem
  execution tool. It comes with support for a multitude of operations including
  tab completion and wildcard support for files, which can be very handy for
  your object storage workflow while working with large number of files.
* [Steampipe](https://steampipe.io/) exposes APIs and services as a
  high-performance relational database, giving you the ability to write
  SQL-based queries to explore dynamic data. Mods extend Steampipe's
  capabilities with dashboards, reports, and controls built with simple HCL.
* [AWS SAML Search](https://github.com/speshak/aws-saml-search) allows
  auto-complete searching of accounts/roles on the AWS SAML login form. It makes
  logging in with the keyboard so much more convenient.
*
  [Jason Stitt's workflow](https://jasonstitt.com/private-packages-codeartifact-poetry-workflow)
  for packaging private Python packages using Poetry to CodeArtifact. I have
  adapted these instructions to maintain my own Python packages.
* [aws-sso-util](https://github.com/benkehoe/aws-sso-util) smooths out the
  rought edges of AWS SSO.
* [botocove](https://github.com/connelldave/botocove) is a Python decorator to
  run against a selection of AWS accounts, or all AWS accounts in an
  organization, concurrently. I am also a maintainer of this tool.
* [aws-sso-cli](https://github.com/synfinatic/aws-sso-cli) is a powerful tool
  for using AWS SSO for the CLI and web console. It's like aws-sso-util but even
  more powerful. It does something I can't do using the stock AWS CLI: log into
  the same AWS SSO instance as two different SSO users at the same time. You may
  need to do this if a client gives you multuple AD users each with access to a
  different set of AWS accounts via AWS SSO.
*
  [Step by step guide to setup port forwarding to remote host](https://github.com/peteragility/ssm-port-forward)
  by Peter Chan makes a good complement to the official documenation.

# Things I haven't tried yet

I may have tried these but not updated the list yet.

* [Leapp](https://github.com/Noovolari/leapp)
* Bees with Machine Guns
* [Scout Suite](https://github.com/nccgroup/ScoutSuite) is an open source
  multi-cloud security-auditing tool, which enables security posture assessment
  of cloud environments.
* [Parliament](https://github.com/duo-labs/parliament)
* [Former2](https://github.com/iann0036/former2)
* https://github.com/turnerlabs/antiope
* https://github.com/jckuester/awsweeper, by the author of awsls, for cleaning
  out an AWS account.
* https://github.com/iann0036/sdk-iam-map
* https://github.com/iann0036/cfn-stack-rename
* See [CloudSecList docs](https://cloudsecdocs.com/aws/devops/resources/scps/)
  for exmaple of using mkdocs to build a wiki-style website. I'd like my site to
  look more like that.
* [aws-allowlister](https://github.com/salesforce/aws-allowlister/)
  automatically compiles an AWS Service Control Policy that ONLY allows AWS
  services that are compliant with your preferred compliance frameworks.
* [aws_public_ips](https://github.com/arkadiyt/aws_public_ips) is a tool to
  fetch all public IP addresses (both IPv4/IPv6) associated with an AWS account.
* [cloudlist](https://github.com/projectdiscovery/cloudlist) is a multi-cloud
  tool for getting Assets (Hostnames, IP Addresses) from Cloud Providers.
* [iamlive](iamlive) generates a basic IAM policy from AWS client-side
  monitoring (CSM)
* [mega-linter](https://github.com/nvuillam/mega-linter) Mega-Linter is an 100%
  Open-Source tool for CI/CD workflows that analyzes consistency and quality of
  different languages and formats.
* [ConsoleMe](https://github.com/Netflix/consoleme) is a web service that makes
  AWS IAM permissions and credential management easier for end-users and cloud
  administrators.
* [Weep](https://github.com/Netflix/weep) is a CLI utility for retreiving AWS
  credentials from ConsoleMe.
* https://github.com/aws-cloudformation/awesome-cloudformation
* [Quail](https://github.com/qube-rt/quail) provides a user interface where your
  team can select configure their EC2 instance and provision it with a click of
  a button in any supported region or account. And you don't have to worry about
  cleaning them up - Quail handles that for you, too!
* [rpCheckup](https://github.com/goldfiglabs/rpCheckup) is an AWS resource
  policy security checkup tool that identifies public, external account access,
  intra-org account access, and private resources. It makes it easy to reason
  about resource visibility across all the accounts in your org.
* [Introspector](https://github.com/goldfiglabs/introspector) is a tool and
  schema for importing cloud infrastructure configuration. The goal is to unlock
  the expressive power of SQL and relational databases to ask questions about
  what is currently deployed in your cloud.
* [actions2aws](https://github.com/glassechidna/actions2aws) "is a GitHub action
  that can grant your workflows access to AWS via an AWS IAM role session. This
  means no need to store long-lived credentials in GitHub and comes with a few
  other benefits." It looks like basically you need to store Github credentials
  in AWS Secrets Manager instead.
* [SSM Tree](https://github.com/brunorubin/aws-ssm-tree) is a tool that provides
  a tree visualization of the parameters hierarchy from AWS System Manager
  Parameter Store.
* [aws2-wrap](https://github.com/linaro-its/aws2-wrap) makes it easier to use
  AWS Single Sign On credentials with tools that don't understand the sso
  entries in an AWS profile.
*
  [IAM Access Analyzer Policy Validation](https://aws.amazon.com/blogs/aws/iam-access-analyzer-update-policy-validation/)
  helps you to construct IAM policies and SCPs that take advantage of
  time-tested AWS best practices. It's like Parliament, but built into AWS.
* [Principal Mapper](https://github.com/nccgroup/PMapper) models the different
  IAM Users and Roles in an account as a directed graph, which enables checks
  for privilege escalation and for alternate paths an attacker could take to
  gain access to a resource or action in AWS.
* [Bucket Stream](https://github.com/eth0izzle/bucket-stream) find interesting
  Amazon S3 buckets by watching certificate transparency llogs.
* [s3audit](https://github.com/scalefactory/s3audit) Checks the settings for all
  S3 buckets in an AWS account for public access
* [cloudtrail-parquet-glue](https://github.com/alsmola/cloudtrail-parquet-glue)
  is terraform module that builds a Glue workflow to convert CloudTrail S3 logs
  to Athena-friendly Parquet format and make them available as a table using a
  Glue Crawler.
* [aws-export-credentials](https://github.com/benkehoe/aws-export-credentials)
  gets AWS credentials from a profile to inject into other programs.
* [Audit Manager](https://aws.amazon.com/audit-manager/) launches an assessment
  that continuously collects and organizes relevant evidence from your AWS
  accounts and resources, such as resource configuration snapshots, user
  activity, and compliance check results.
*
  [The CloudFormation Command Line Interface (CLI)](https://docs.aws.amazon.com/cloudformation-cli/latest/userguide/what-is-cloudformation-cli.html)
  is an open-source tool that enables you to develop and test AWS and
  third-party extensions, such as resource types or modules, and register them
  for use in AWS CloudFormation
*
  [IAM Access Analyzer](https://aws.amazon.com/blogs/security/iam-access-analyzer-makes-it-easier-to-implement-least-privilege-permissions-by-generating-iam-policies-based-on-access-activity/)
  now generates least-privilege permissions based on access activity.
* [Access Analyzer - Batch Policy Validator](https://github.com/z0ph/aa-policy-validator)
analyzes using AWS Access Analyzer - Policy Validation all your account customer
managed IAM policies.
*
  [Assisted Log Enabler]/(https://github.com/awslabs/assisted-log-enabler-for-aws)
  Find resources that are not logging, and turn them on. Supports VPC Flow Logs,
  CloudTrail, EKS Audit and Authenticator Logs, S3 Access Logs, and Route 53
  Query Logs.
* [yor](https://github.com/bridgecrewio/yor) is an open-source tool that helps
  add informative and consistent tags across infrastructure-as-code frameworks
  such as Terraform, CloudFormation, and Serverless.
* [llama](https://github.com/nelhage/llama)'s goal goal is to make it easy to
  outsource compute-heavy tasks to Lambda, with its enormous available
  parallelism, from your shell. Llama includes llamacc, a drop-in replacement
  for gcc or clang which executes the compilation in the cloud, allowing for
  considerable speedups building large C or C++ software projects.
*
  [IAM Policy Validator for AWS CloudFormation](https://github.com/awslabs/aws-cloudformation-iam-policy-validator)
  is a command line tool that takes a CloudFormation template, parses the IAM
  policies attached to IAM roles, users, groups, and resources then runs them
  through IAM Access Analyzer validation checks.

* cfn-config
* autostacker24
* https://github.com/okigan/awscurl
* https://github.com/k1LoW/awspec
* https://github.com/toniblyx/prowler
* https://dce.readthedocs.io/en/latest/quickstart.html

* https://github.com/DavidMuller/aws-requests-auth
* https://github.com/nordcloud/cognitocurl
* https://github.com/aws-cloudformation/cloudformation-guard
* https://github.com/common-fate/iamzero
* https://aws.amazon.com/solutions/implementations/customizations-for-aws-control-tower/

* [s3wipe](https://github.com/eschwim/s3wipe) is a rapid parallelized AWS S3 key
  & bucket deleter.
* [tail-stack-events](https://github.com/tmont/tail-stack-events) is a
  convenient little CLI script (written in Node) to tail the latest AWS
  CloudFormation stack events. According to the open Github issues it doesn't
  support profiles yet, but it looks promising. Like the AWS SAM CLI without
  being tied to SAM's deployment model.
* [awsrm](https://github.com/jckuester/awsrm) simplifies deleting over 250 AWS
  resource types across multiple accounts and regions. It takes awsls as input.
* [delete_vpc](https://github.com/lianghong/delete_vpc) is a shell script to
  delete an AWS VPC and its dependencies. All the networking resources you would
  expect, plus EC2 instances, are listed as dependencies, but what about other
  services that use VPCs such as Redshift or CodeBuild to name a random couple?
* [efsu](https://github.com/glassechidna/efsu) is for accessing AWS EFS from
  your machine without a VPN. It achieves this by deploying a Lambda function
  and shuttling data between your machine and EFS via that function. You can use
  it to copy a file from EFS to your local machine!
* [stackit](https://github.com/glassechidna/stackit) is a CLI tool to
  synchronously and idempotently operate on AWS CloudFormation stacks. Provides
  commands up, down, tail and outputs. By glassechidna, the same author of
  actions2aws.
* [aws-cfn-update](https://github.com/binxio/aws-cfn-update) is a utility to
  programmatically update CloudFormation templates. Could be useful for
  automating imports.
* [coto](https://github.com/sentialabs/coto) is like boto3, but scraping the AWS
  console. There exist some administrative tasks for which there is no public
  API, and there exist some AWS tasks that still require the AWS Account Root
  User.
* [bash-my-aws](https://github.com/bash-my-aws/bash-my-aws) Bash-my-AWS is a
  simple but powerful set of CLI commands for managing resources on Amazon Web
  Services. They harness the power of Amazon's AWSCLI, while abstracting away
  verbosity. The project implements some innovative patterns but (arguably)
  remains simple, beautiful and readable.
* [cfn-events](https://github.com/rvedotrc/cfn-events) watches AWS
  CloudFormation stack events and wait for completion. A standalone tail
  command that just works.
* [better-boto](https://pypi.org/project/better-boto/) is a collection of helper
  functions to make using AWS Boto easier. For example, it added a
  create_or_update method to the AWS CloudFormation client. It also provides
  some helpers for dealing with pagination operations and some help using AWS
  Organizations.
* [tail-stack-events](https://github.com/tmont/tail-stack-events) is a
  convenient little CLI script written in Node to tail the latest AWS
  CloudFormation stack events.
* [ThreatModel for S3](https://github.com/trustoncloud/threatmodel-for-aws-s3).
  See the blog: The last S3 security document that we’ll ever need, and how to
  use it.
* [Superwerker](https://superwerker.cloud/) lets you set up a secure
  multi-account AWS Cloud environment in just a few clicks. It has been
  developed to follow the latest best practices in cloud security and
  efficiency, by AWS Advanced Partners who have decades of experience in the
  field of cloud computing.
* [Change log of AWS IAM permissions](https://www.awsiamchanges.com/)
*
  [AWSUtility::CloudFormation::CommandRunner](https://aws.amazon.com/blogs/mt/running-bash-commands-in-aws-cloudformation-templates/)
  is a CloudFormation resource type that allows users to run Bash commands in
  any CloudFormation stack. Any output written using the command to the reserved
  file /command-output.txt can be referenced anywhere in your template by using
  !Fn::GetAtt Command.Output. See FAQ for comparison to Lambda, Custom Resources
  and Macros.
*
  [Pyplate](https://github.com/awslabs/aws-cloudformation-templates/tree/master/aws/services/CloudFormation/MacrosExamples/PyPlate)
  is a CloudFormation macro to run arbitrary Python code in your CloudFormation
  templates. It is useful for generating values that are impossible using the
  CloudFormation intrinsic functions.
* [aws-auth](https://aws-auth.js.org/) allows you to programmatically
  authenticate into AWS accounts through IAM roles in a multi-account AWS
  organization setup.
* [aws-inventory](https://github.com/nccgroup/aws-inventory) tries to discover
  all AWS resources created in an account. Uses botocore's service definition
  files to stay up to date.
* [awsiam.info](https://www.awsiam.info/) is like ec2instances.info but for IAM permissions.
* [Aegea](https://github.com/kislyuk/aegea) is a command line interface (CLI)
  that provides a set of essential commands and terminal dashboards for
  operators of Amazon Web Services (AWS) accounts. Aegea lets you build AMIs and
  Docker images using the cloud-init config management package, manage config
  roles, launch and monitor instances and services, and manage AWS resources
  including ELB, RDS, and AWS Batch. Aegea is designed to be used in conjunction
  with the existing functionality of the AWS CLI and boto3.
*
  [CloudWatch Custom Widgets Samples](https://github.com/aws-samples/cloudwatch-custom-widgets-samples).
  A Custom Widget is a CloudWatch Dashboards widget that can display virtually
  anything you want. Custom Widgets enables you to add custom visualizations,
  display information from multiple sources or add custom controls like buttons
  to take actions directly in a CloudWatch Dashboard.
* [AWS Permissions Cloud](https://aws.permissions.cloud/) is a nicer way to view all the IAM permissions.
* [awsservices.info](https://awsservices.info/) is another version of the AWS Service Matrix.
* [fck-nat](https://github.com/AndrewGuenther/fck-nat/) is an AMI that is a
  cheaper NAT than the managed NAT.
* [ssmsh](https://github.com/bwhaley/ssmsh) brings bashlike commands and folder
  navegation to to SSM.
* [gotoaws](https://github.com/hupe1980/gotoaws) connects to your EC2 instance
  or ECS container without the need to open inbound ports, maintain bastion
  hosts, or manage SSH keys. A nice wrapper around session manager. Similar to
  aws-gate. Discovered in this video. https://www.youtube.com/watch?v=uLtx1PUUZJQ
* [AWS serverless patterns](https://github.com/aws-samples/serverless-patterns)
  contains serverless patterns showing how to integrate services services using
  infrastructure-as-code (IaC). You can use these patterns to help develop your
  own projects quickly.
* [PolicyGlass](https://github.com/CloudWanderer-io/PolicyGlass) allows you to
  analyse one or more AWS policies' effective permissions in aggregate, by
  restating them in the form of PolicyShards which are always Allow, never Deny.
* [Pacu])(https://github.com/RhinoSecurityLabs/pacu) allows penetration testers
  to exploit configuration flaws within an AWS account, using modules to easily
  expand its functionality. Current modules enable a range of attacks, including
  user privilege escalation, backdooring of IAM users, attacking vulnerable
  Lambda functions, and much more.
* [Trailscraper](https://github.com/flosell/trailscraper) is a command-line tool
  to get valuable information out of AWS CloudTrail and a general purpose
  toolbox for working with IAM policies. It can get CloudTrail events matching a
  filter from the CloudTrail API, it can download from a trail archive bucket
  for single-account and for organization trails, it can filter the downloaded
  logs, it can generate IAM policy from CloudTrail events, and extend policy by
  guessing matching actions.
*
  [iam-policy-json-to-terraform](https://github.com/flosell/iam-policy-json-to-terraform)
  converts an IAM Policy in JSON format into a Terraform configuration.
* [Cloudtracker](https://github.com/duo-labs/cloudtracker) helps you find
  over-privileged IAM users and roles by comparing CloudTrail logs with current
  IAM policies. It can list actors, list their actions, and show which users
  assumed which roles.
*
  [AWS SSM Automation Runbook to Setup AWS Config with Amazon Athena and Amazon QuickSights](https://github.com/aws-samples/aws-management-and-governance-samples/tree/master/AWSConfig/AWS-Config-Visualization)
  deploys a SSM Automation runbook called Config-QuickSight-Visualization that
  can be used to setup AWS Config to be used with Amazon Athena and setup Amazon
  Quicksights to be able to create visualize dashboards. See blog post
  [Automate AWS Config data visualization with AWS Systems Manager](https://aws.amazon.com/blogs/mt/automate-aws-config-data-visualization-with-aws-systems-manager/).
  *
  [Consistent point-in-time restore for Amazon S3 buckets](https://aws.amazon.com/blogs/storage/consistent-point-in-time-restore-for-amazon-s3-buckets/).
  In this blog post, we showed how Amazon S3 Event Notifications with
  EventBridge can be used to create near real-time log of S3 object changes. We
  also showed how you can query this log and use S3 Batch Operations to restore
  S3 objects to a consistent point in time.
* [Flowdog](https://github.com/aidansteele/flowdog) is an application/framework
  for inspection and manipulation of network traffic in AWS VPCs. Packets routed
  to or from the Internet, between VPCs, between subnets can all be
  programmatically inspected or modified in great detail.
* https://aws.amazon.com/blogs/security/disabling-security-hub-controls-in-a-multi-account-environment/
*
  [Enable Security Hub PCI DSS standard across your organization and disable specific controls](https://aws.amazon.com/blogs/security/enable-security-hub-pci-dss-standard-across-your-organization-and-disable-specific-controls/)
*
  [Disabling Security Hub controls in a multi-account environment](https://aws.amazon.com/blogs/security/disabling-security-hub-controls-in-a-multi-account-environment/)
*
  https://aws.amazon.com/blogs/mt/automate-aws-config-data-visualization-with-aws-systems-manager/
* [AWS Fuzzy Finder](https://github.com/pmazurek/aws-fuzzy-finder) makes the
  process of finding the IPs and SSH'ing into your EC2 instances super fast and
  easy. It will connect with AWS, automatically grab all the instances you have
  access to, and present them to you in a fuzzy searchable way.
* [Mapping Tool](https://mappingtool.dev/) lets you quickly Build and Debug
  AppSync & API Gateway Mapping Templates written in VTL (Apache Velocy Template
  Language).
* [Resoto](https://github.com/someengineering/resoto) is "housekeeping for
  clouds". Resoto indexes resources, captures dependencies and maps out your
  infrastructure in a graph so that it’s understandable for a human. The graph
  contains metrics for each resource. Developers and SREs can search the graph
  with a query language, and create alerting and clean-up workflows. Metrics can
  be aggregated and exported to a time series database like Prometheus.
*
  [AWS Lambda Event Resources](https://michaelbrewer.github.io/aws-lambda-events/)
  tries to collect as many resources on AWS Lambda events from schema, examples
  to code libraries.
* [Granted](https://github.com/common-fate/granted) is a command line interface
  (CLI) application which simplifies access to cloud roles and allows multiple
  cloud accounts to be opened in your web browser simultaneously. It seems like
  a way to control Firefox containers from the command line.
*
  [BYO Organization Tool](https://aws.amazon.com/blogs/architecture/field-notes-building-multi-region-and-multi-account-tools-with-aws-organizations/)
  is like Botocove but in Go. It's demo code from an AWS blog post about
  querying across the organization.
* [Access Undenied](https://github.com/ermetic/access-undenied-aws) parses AWS
  AccessDenied CloudTrail events, explains the reasons for them, and offers
  actionable fixes.
* [ElectricEye](https://github.com/jonrau1/ElectricEye) continuously monitors
  your AWS services for configurations that can lead to degradation of
  confidentiality, integrity or availability. All results can be exported to
  Security Hub, JSON, CSV, Databases, and more for further aggregation and
  analysis.
* [AWS Data Wrangler](https://github.com/awslabs/aws-data-wrangler) is Pandas on
  AWS - Easy integration with Athena, Glue, Redshift, Timestream, Neptune,
  OpenSearch, QuickSight, Chime, CloudWatchLogs, DynamoDB, EMR, SecretManager,
  PostgreSQL, MySQL, SQLServer and S3 (Parquet, CSV, JSON and EXCEL).
*
  [Automating safe, hands-off deployments](https://aws.amazon.com/builders-library/automating-safe-hands-off-deployments/)
  from the Amazon Builders' Library. In this article, we walk through the steps
  a code change goes through in a pipeline at Amazon on its way to production. A
  typical continuous delivery pipeline has four major phases—source, build,
  test, and production (prod). We’ll dive into the details of what happens in
  each of these pipeline phases for a typical AWS service, and provide you with
  an example of how a typical AWS service team might set up one of their
  pipelines.
* [Cloudash](https://cloudash.dev/) allows you to troubleshoot issues faster
  than you can open CloudWatch console. Serverless observability is hard, but it
  doesn't have to be.
* [The Athena Guide](https://the.athena.guide/) is Athena's
  missing manual. In its own words: "Athena is an in-depth guide to Amazon
  Athena. Athena is a serverless query service for data on S3, but there is a
  lot behind that description. Here you will find articles that explain the not
  so obvious aspects of how to use the service to its full potential, including
  how and why to partition your data, how to get the best performance, and
  lowest cost, and how to use it as the engine for your data lake."
*
  [open-url-in-container](https://github.com/honsiorovskyi/open-url-in-container)
  is a Firefox extension that enables support for opening links in specific
  containers using custom protocol handler. This could make it easier to automate opening different AWS sessions in containers in the browser.
* [aws-ssoreporting](https://github.com/dustintodd123/aws-ssoreporting) is a
  "down and dirty script to report AWS SSO users and groups". It works around
  the fact that the SSO's ListUsers API doesn't actually list users. I reported
  the [issue](https://github.com/aws/aws-sdk/issues/109) and someone just shared
  their solution there. No response from AWS, unfortunately.
* [cfplot](https://github.com/jaredtrog/cfplot) connects to AWS, grab the output
  of describe_stacks and generate a horizontal waterfall diagram similar to a
  webpage load waterfall diagram.
* [Smithy](https://github.com/awslabs/smithy) is a protocol-agnostic interface
  definition language and set of tools for generating clients, servers, and
  documentation for any programming language. It's used to generated the SDKs
  for JavasScript, Go, and Rust.
*
  [A Review of the AWS Security Model](https://www.nojones.net/posts/a-review-of-the-aws-security-maturity-model).
  AWS have released their own security maturity model, which contains a lot of
  detail on their take as to how to secure your AWS estate. Does it stack up
  against what we’re seeing in real-world attacks, or the approaches being
  suggested by the rest of the AWS security community? Unfortunately, Nick Jones
  is not convinced, and here he lays out why.
*
  [AWS Roles Anywhere Sample Script](https://nerdydrunk.info/aws:roles_anywhere?ck_subscriber_id=512841606).
  To use AWS Identity and Access Management Roles Anywhere for authentication to
  AWS from your workloads that run outside of AWS such as servers, containers,
  and applications, you first create a trust anchor and profile through the IAM
  Roles Anywhere console.
* [AWS Auto Cleanup](https://github.com/servian/aws-auto-cleanup) helps clean up
  your AWS resources, keeping your AWS accounts neat and tidy whilst reducing
  costs. AWS Auto Cleanup uses a combination of user-defined allowlists and
  time-based policies to delete resources that are no longer needed.
* [aws_list_all](https://github.com/JohannesEbke/aws_list_all) List all
  resources in an AWS account, all regions, all services(*). Writes JSON files
  for further processing.
* [aws-rotate-key](https://github.com/stefansundin/aws-rotate-key) rotates your
  AWS access key. If you still need to use IAM users, then at least it's good to
  have a way to reotate the keys more easily.
*
  [How to delete a stuck custom resource](https://aws.amazon.com/premiumsupport/knowledge-center/cloudformation-lambda-resource-delete/)
  is a support article fully titled "How do I delete a Lambda-backed custom
  resource that's stuck in DELETE_FAILED status or DELETE_IN_PROGRESS status in
  CloudFormation?". I'll save this for the next time I work on custom resource
  Lambdas.
* [AWS Lambda Typing](https://github.com/MousaZeidBaker/aws-lambda-typing)
  provides type hints for AWS Lambda event, context, and response objects.
*
  [AWS Lambda Powertools for Python](https://github.com/awslabs/aws-lambda-powertools-python)
  is a suite of utilities for AWS Lambda Functions that makes tracing with AWS
  X-Ray, structured logging and creating custom metrics asynchronously easier.
  Include typing for Lambda context.
* [imds-credential-server](https://github.com/benkehoe/imds-credential-server)
  runs a server compliant with the EC2 IMDSv2 interface in order to vend AWS
  credentials, primarily to export credentials into locally-run containers.
*
  [S3 Intelligent-Tiering: What It Takes To Actually Break Even](https://www.duckbillgroup.com/blog/s3-intelligent-tiering-what-it-takes-to-actually-break-even/)
  shows how to analyze S3 storage classes based on the break-even costs compared
  to S3 standard.
*
  [What You Need to Know About Ransomware in AWS](https://www.firemon.com/what-you-need-to-know-about-ransomware-in-aws/)
  describes a talk from AWS re:Inforce about ransomware attacks in AWS and how
  to avoid them.
*
  [AWS Limit Monitor Solution](https://github.com/aws-solutions/aws-limit-monitor)
  is a reference implementation that provides a foundation for monitoring AWS
  service limits. Customers can leverage the solution to monitor limits across
  services supported by Amazon Trusted Advisor; in multiple regions and multiple
  AWS accounts. The solution integrates with Amazon SNS and Slack to notify
  customers for service limits approaching thresholds.
*
  [Customizations for AWS Control Tower Solution](https://github.com/aws-solutions/aws-control-tower-customizations)
  combines AWS Control Tower and other highly-available, trusted AWS services to
  help customers more quickly set up a secure, multi-account AWS environment
  based on AWS best practices.
* [AWS Simple EC2 CLI](https://github.com/awslabs/aws-simple-ec2-cli): In order
  to launch a new EC2 instance, customers need to specify a lot of options, and
  it can be a slow and overwhelming task. It requires users to have an initial
  network stack (VPC-Id/Subnet-Id/Security-Groups), remote login, and many more.
  Often times, we require EC2 instance for adhoc testing for a short period of
  time without requiring complex networking infrastructure in place. AWS Simple
  EC2 CLI aims to solve this issue and make it easier for users to launch,
  connect and terminate EC2 instances with a single command.
* [IAM Floyd](https://github.com/udondan/iam-floyd) is an AWS IAM policy
  statement generator with fluent interface. # How to passively discover AWS
  tools
* [CloudFormation Seed](https://github.com/Innablr/cloudformation-seed) is a
  script to automate CloudFormation deployments.
* [Sceptre](https://github.com/Sceptre/sceptre) is a tool to drive AWS
  CloudFormation. It automates the mundane, repetitive and error-prone tasks,
  enabling you to concentrate on building better infrastructure.
* [AWS Role Extension (AWS RolEx)](https://github.com/jkueh/aws-rolex) is a way
  to type to filter the SAML role selection page. I prefer aws-saml-search, but
  this is prettier if you like cats.
* [BeSA](https://become-a-solutions-architect.github.io/) This is a unique
  opportunity for you to be mentored by AWS Solution Architects to acquire
  skills to become a Solutions Architect. Many people reach out to us to get
  started on learning AWS to become Solutions Architect, or, simply need help
  with achieving certifications for career progression. In this free pilot
  program, we will focus on technical and behavioural concepts for becoming a
  successful Solutions Architect, help you upskill for certifications, and
  provide interview coaching support for opportunities inside and outside AWS.
*
  [AWS Insights Mod for Steampipe](https://github.com/turbot/steampipe-mod-aws-insights)
  is an AWS dashboarding tool that can be used to view dashboards and reports
  across all of your AWS accounts. Dashboards are available for 15+ services,
  including CloudTrail, EC2, IAM, RDS, S3, VPC, and more. Discovered via Nsikan
  Essien's article on InfoQ:
  [Accelerated Multi-Account Auditing and Compliance in AWS with Steampipe, HCL and SQL](https://www.infoq.com/news/2022/09/aws-dashboards-as-code/).
* https://github.com/awslabs/aws-lambda-web-adapter
*
  [Baseline Environment on AWS (BLEA)](https://github.com/aws-samples/baseline-environment-on-aws/)
  is a set of reference CDK template to establish secure baseline on
  standalone-account or ControlTower based multi-account AWS environment. This
  solution provides basic and extensible guardrail with AWS security services
  and end-to-end sample CDK code for typical system architecture.
* [Scarr](https://github.com/kkuchta/scarr): If you want to set up a
  production-grade flat file site, a reasonable way to accomplish this would be
  to load your files to S3, put cloudfront in front of that for caching, use
  route53 for domain registration + DNS, and ACM for your TLS certificate. This
  tool automates all of that from registering the domain to uploading files.
* [AWS Jam](https://jam.awsevents.com/) puts your skills to the test. Solve
  challenges that emulate real AWS use-cases across a variety of subject
  matters. Work to complete challenges across common AWS use-cases and
  operational tasks to earn points. Compete with rivals for the coveted top
  place on the leaderboard.
* [cloudtrail2sightings](https://github.com/zmallen/cloudtrail2sightings)
  converts cloudtrail data to MITRE ATT&CK Sightings. This project assumes any
  Cloudtrail data it processes to be from an incident, or from known attacks on
  your environment. For generating Cloudtrail data attached to known attack
  techniques, you can use stratus-red-team or from flaws.cloud public dataset of
  logs from attacks in this environment.
* [Incident Response in AWS](https://www.chrisfarris.com/post/aws-ir/) is a post
  by Chris Farris intended to help those already familiar with the principles of
  Incident Response to understand what to do when the incident involves the AWS
  Control Plane.
* [aws-keys-sectool](https://github.com/toshke/aws-keys-sectool) does something
  obvious in hindsight: making sure that any long-lived credentials on your
  machine are access restricted to your own IP address.
* [Amazon Ion](https://amzn.github.io/ion-docs/) is a richly-typed,
  self-describing, hierarchical data serialization format offering
  interchangeable binary and text representations. The text format (a superset
  of JSON) is easy to read and author, supporting rapid prototyping. The binary
  representation is efficient to store, transmit, and skip-scan parse. The rich
  type system provides unambiguous semantics for long-term preservation of data
  which can survive multiple generations of software evolution.
* [cloud-nuke](https://github.com/gruntwork-io/cloud-nuke) a CLI tool to delete
  all resources in an AWS account.
* [chamber](https://github.com/segmentio/chamber) is a tool for managing
  secrets. Currently it does so by storing secrets in SSM Parameter Store, an
  AWS service for storing secrets. For detailed info about using chamber, read
  [The Right Way To Manage Secrets](https://aws.amazon.com/blogs/mt/the-right-way-to-store-secrets-using-parameter-store/).
  *
  [How to get started with OrgFormation](https://bahr.dev/2022/02/07/org-formation/).
  OrgFormation is a community driven Infrastructure as Code (IaC) tool for
  managing AWS Organizations. It’s open source on GitHub and available on NPM.
  With IaC you reduce the friction that engineers experience when creating new
  accounts. You can also set up rules and guidelines for your whole AWS
  organization. OrgFormation has many features, like restricting unused regions
  and large EC2 instances, offering a nice login experience with AWS SSO, and
  managing service quotas via code. In this article we will set up an
  OrgFormation CI/CD pipeline. It will help us with creating accounts, setting
  up billing alerts, and using AWS SSO to sign in to accounts.
* [Grayhat Warfare Public Bucket Search](https://buckets.grayhatwarfare.com/) is
  a free tool that lists open s3 buckets and helps you search for interesting
  files.
* [CloudFormation Workshop](https://github.com/aws-samples/cfn101-workshop)
  educates builders about the features of AWS CloudFormation and how to get
  started building quickly. A background in CloudFormation, command line, git,
  and development workflows is not required.
* [AWS Security Survival Kit](https://github.com/zoph-io/aws-security-survival-kit)
* [awsapilib](https://github.com/schubergphilis/awsapilib) is a python library
  exposing services that are not covered by the official boto3 library but are
  driven by undocumented APIs. The new coto?
* [aws-error-utils](https://github.com/benkehoe/aws-error-utils) makes
  `botocore.exceptions.ClientError` easier to deal with. All AWS service
  exceptions are raised by boto3 as a botocore.exceptions.ClientError, with the
  contents of the exception indicating what kind of exception happened. This is
  not very pythonic, and the contents themselves are rather opaque, most being
  held in dicts rather than as properties. The functions in this package help
  dealing with that, to make your code less verbose and require less
  memorization of ClientError contents.
* [alternat](https://github.com/1debit/alternat) is a high availability
  implementation of AWS NAT instances. NAT instances aren't for everyone. You
  might benefit from alterNAT if NAT Gateway data processing costs are a
  significant item on your AWS bill. If the hourly cost of the NAT instances
  and/or the NAT Gateways are a material line item on your bill, alterNAT is
  probably not for you. As a rule of thumb, assuming a roughly equal volume of
  ingress/egress traffic, and considering the slight overhead of operating NAT
  instances, you might save money using this solution if you are processing more
  than 10TB per month with NAT Gateway.
*
  [Sustainable Personal Accounts](https://github.com/reply-fr/sustainable-personal-accounts)
  promotes the idea that each AWS practitioner should have direct access to some
  AWS account, so as to practice almost freely on the platform and to accelerate
  innovation of their company. At the same time, in corporate environment there
  is a need for enforcing policies, for managing costs and for fostering
  software automation. For this reason, with Sustainable Personal Accounts
  (SPA), we introduce maintenance windows on AWS accounts and alerts. Central
  teams can customise code ran on each maintenance window, for example to set
  billing alerts, to deploy SIEM agents, and to purge cloud resources.
*
  [Amazon Redshift Research Project](https://www.amazonredshiftresearchproject.org/)
  is not your typical site, with advice like "pick a good distribution key".
  This is low-level, detailed, comprehensive research. If you do any kind of
  serious work with Redshift, the content here will blow you away. The core
  content are the white papers, but there are a growing number of Redshift
  tracking/monitoring pages. Discovered via a comment on the [The Register]()
*
  [EBS Direct API Secuity Tooling](https://github.com/crypsisgroup/ebs-direct-sec-tools)
  are fun tools around the EBS Direct API to download snapshots, dump blocks,
  scan secrets, and diff secrets.
*
  [Amazon DynamoDB Permissions Workshop Code](https://github.com/aws-samples/dynamodb-permissions-workshop).
  Welcome to the Amazon DynamoDB Permissions workshop, the full step by step
  workshop can be found on dynamodb-security-101 workshop hosted in Event Box.
* [Deequ](https://github.com/awslabs/deequ) is a library built on top of Apache
  Spark for defining "unit tests for data", which measure data quality in large
  datasets. Most applications that work with data have implicit assumptions
  about that data, e.g., that attributes have certain types, do not contain NULL
  values, and so on. If these assumptions are violated, your application might
  crash or produce wrong outputs. The idea behind deequ is to explicitly state
  these assumptions in the form of a "unit-test" for data, which can be verified
  on a piece of data at hand. If the data has errors, we can "quarantine" and
  fix it, before we feed it to an application.
* [AWS Data Tools](https://github.com/timoguin/aws-data-tools-py) offers the
  OrganizationDataBuilder class to build a full model of an AWS Organization. It
  handles recursing the organizational tree and populating any relational data
  between the various nodes, e.g., parent-child relationships between an OU and
  an account. Parallel operations are not supported, so everything runs
  serially.
* [AWS Baseline](https://github.com/timoguin/aws-baseline) defines esssential
  baseline resources that should exist in every AWS account. This project is
  intended to be a combination of best practices documentation with a
  step-by-step guide on setting up an AWS account in A Right Way™ (or at least
  that's what Tim O'Guin is calling it).
* [AWS Organized](https://github.com/aws-samples/aws-organized) allows you to
  visualise and make changes to your AWS Organization using folders and files.
  Instead of making changes directly you build up a migration which can then be
  reviewed before being applied.
*
  [S3 Deletion Visualizer](https://tomforb.es/visualizing-how-s3-deletes-1-billion-objects-with-athena-and-rust/)
  Tom Forbes had the chance to delete 1 petabyte of data spread across 1 billion
  objects from S3. Well, actually 940 million, but close enough to the
  click-baitable 1 billion. He thought it would be interesting challenge to try
  and visualize the execution of these deletions and possibly gain some insights
  into how S3 Lifecycle Policies work under the hood.
* [update-aws-ip-ranges](https://github.com/aws-samples/update-aws-ip-ranges) is
  a Lambda function that automatically create or update AWS resource with AWS
  service's IP ranges from the ip-ranges.json file. You can configure which
  service and region to get range. You can also configure to which resources you
  want to create or update with those ranges. It supports WAF IPSet and VPC
  Prefix List.
* [headless-sso](https://github.com/mziyabo/headless-sso) runs AWS SSO login
  headlessly - that is, without needing to launch a GUI web browser. Supports
  only hardware MFA at the moment.
* [awscost](https://github.com/toyama0919/awscost/) gives a tabular view in the
  console of AWS costs. It's a lot more convenient that using the bare Cost
  Explorer API!
*
  [AWS CloudFormation Resource Provider Python Plugin](https://github.com/aws-cloudformation/cloudformation-cli-python-plugin)
  allows you to author CloudFormation resource providers in the Python
  programming language.
*
  [Security Hub Score Generator](https://github.com/aws-samples/aws-securityhub-score-generator)
  is an AWS-published script for computing the security score as the Security
  Hub console does. For some reason the score doesn't have an API. Maybe you
  could use it to build a scoreboard by running it across all the accounts in an
  organization.
* [Execute Node.js in AWS Lambda wrapper scripts](https://github.com/WojciechMatuszewski/lambda-wrapper-scripts-abuse) contains an example of how one might use wrapper scripts to push the expensive computations to AWS Lambda Init phase.
* [AWS IP Ranges History Tracker](https://github.com/joetek/aws-ip-ranges-json)
  for changes to the ip-ranges.json file.
* [AWS re:Skill](https://awsreskill.com/) is a gamified AWS learning platform.
*
  [AWS Toolkit for Azure DevOps](https://github.com/aws/aws-toolkit-azure-devops)
  adds tasks to Azure Devops to work with S3, Elastic Beanstalk, CodeDeploy,
  Lambda, CloudFormation, SQS, SNS, and run commands using the AWS PowerShell
  module and the AWS CLI.
* [AWS Break Glass Role](https://github.com/awslabs/aws-break-glass-role)
  Creates a break glass role for emergency use in order to limit production
  console access. Configures automatic alerts and logging of activities in the
  role to secure its use in production environments.
* [AWS pwn](https://github.com/dagrz/aws_pwn) is a collection of AWS penetration
  testing junk. Has examples of reconnaissance, exploitation, stealth,
  exploration, elevation, persistance, and exfiltration.
*
  [AWS Resource Providers](https://github.com/org-formation/aws-resource-providers)
  is a community driven repository where you can find CloudFormation Resource Type
  Providers for different purposes (including org-formation ones).
*
  [The Hitchhiker's Guide to PlantUML: Create Real Life AWS Diagrams](https://crashedmind.github.io/PlantUMLHitchhikersGuide/aws/aws.html)
  shows how to recreate diagrams from the official AWS Architecture Blog using
  PlantUML syntax.
* [AWS Icons for PlantUML](https://github.com/awslabs/aws-icons-for-plantuml)
  provides sprites, macros, and other includes that represent AWS services and
  resources in PlantUML syntax.
* [ADMT](https://github.com/aws-samples/data-migration-tool-for-s3) (A data
  migration tool, formerly goSync) is a data migration tool developed by Golang.
  It can upload data to S3, download data to local directory, copy data between
  buckets/prefixes, or between directories. It also supports file attributes
  retain in S3 uploading and downloading, attribute check, md5 check after data
  copy, incremental copy. It is only 10MB size and doesn’t require installation.
  It works well in k8s, especially in AI/ML, HPC scenarios.
*
  [AWS Fargate Right-sizing Dashboard](https://github.com/mreferre/container-insights-custom-dashboards/tree/master/fargate-right-sizing)
  uses CloudWatch Logs Insights to scan and analyze performance logs collected
  from the cluster you want to optimize.
* [aws-runas](https://github.com/mmmorris1975/aws-runas) is a friendly way to do
  AWS STS AssumeRole operations, so you can perform AWS API actions using a
  particular set of permissions. Includes support for IAM user credentials and
  SAML SSO, including MFA for both. Works off of profile names configured in the
  AWS SDK configuration file.
* [AWS Account Controller](https://github.com/iann0036/aws-account-controller)
  gives self-service creation and deletion of sandbox-style accounts. Abandoned
  since the CloseAccount method went public, but that API is so restricted that
  we still need a tool like this.
* [AWS Management Account Vending Machine (MAVM)](https://github.com/superluminar-io/mavm)
  Create new AWS management accounts on the fly and clean up and close accounts
  afterwards again. Fully automated. Special thanks for inspiration and parts of
  the code go to Ian McKay and his AWS Account Controller, and the awsapilib.
* [Which AWS Certification exam should I sit?](https://dev.to/aws-builders/which-aws-certification-exam-should-i-sit-hah)
  Tom Milner answers the question with data, showing which services the exams
  cover and how much overlap there is between each exam pair. Great for career
  path planning.
* [Curso IaC con CDK en AWS](https://www.linkedin.com/posts/rubenjgarciab_aws-cdk-iac-activity-7035918961057656832-NS8i?utm_source=share&utm_medium=member_desktop)
  is a Spanish-language course that introduces the CDK (Cloud Development Kit)
  and builds up to advanced development topics. It's currently in preview and is
  developed by my ex-colleague Rubén García. ¡Saludos!
* [IAM APE](https://github.com/orcasecurity/orca-toolbox/tree/main/iam-ape), or
  AWS Policy Evaluator, takes all of your AWS IAM policies attached to a User,
  Group, or Role object, and presents you with a single policy, summarizing all
  of their actual permissions, taking into account permissions, denials,
  inherited permissions and permission boundaries.
* [Mountpoint for Amazon S3](https://github.com/awslabs/mountpoint-s3) is a simple, high-throughput file client for [mounting an Amazon S3 bucket as a local file system](https://aws.amazon.com/blogs/storage/the-inside-story-on-mountpoint-for-amazon-s3-a-high-performance-open-source-file-client/). With Mountpoint for Amazon S3, your applications can access objects stored in Amazon S3 through file operations like open and read. Mountpoint for Amazon S3 automatically translates these operations into S3 object API calls, giving your applications access to the elastic storage and throughput of Amazon S3 through a file interface. Mountpoint for Amazon S3 is optimized for read-heavy workloads that need high throughput. It intentionally does not implement the full POSIX specification for file systems. See SEMANTICS.md for a detailed description of the client's behavior and POSIX support.
* [orgtool](https://github.com/aws-samples/aws-organizations-tool) is a
  configuration management tool set for AWS Organizations written in python.
  This tooling enable the configuration and management of AWS Organization with
  code.
* [Service Screener](https://github.com/aws-samples/service-screener) allows AWS
  customers to automate checks on their environment and services based on AWS
  best practices and provide recommendations on how to improve.
* [cfn-include](https://www.npmjs.com/package/cfn-include) is a preprocessor for
  CloudFormation templates which extends CloudFormation's intrinsic functions.
  For example, `Fn::Include` provides a convenient way to include files, which
  can be local, a URL or on an S3 bucket (with proper IAM authentication if
  necessary). It supports both JSON and YAML as input and output format.
  CloudFormation's tag syntax for YAML (e.g. !GetAtt) is supported as well.
* [Amazon Cognito Passwordless Auth](https://github.com/aws-samples/amazon-cognito-passwordless-auth)
  demonstrates several patterns to support passwordless authentication and
  provides reference implementations for FIDO2 (WebAuthn), Magic Link, and SMS
  OTP Step Up.
* [cfnctl](https://github.com/rogerwelin/cfnctl) is a CLI that brings the
  Terraform cli experience to Cloudformation. As the author says, "You'll never
  need to use the AWS Console for managing stacks again!". The SAM CLI already
  exists, but there is an opportunity to provide more detail around
  changesets than `sam deploy --confirm-changeset` currently provides.
* [Automated Security Response on AWS](https://github.com/aws-solutions/aws-security-hub-automated-response-and-remediation)
  is an add-on solution that works with AWS Security Hub to provide a
  ready-to-deploy architecture and a library of automated playbooks. The
  solution makes it easier for AWS Security Hub customers to resolve common
  security findings and to improve their security posture in AWS.
* [Antiope](https://github.com/turnerlabs/antiope) is is intended to be an open
  sourced framework for managing resources across hundreds of AWS Accounts. From
  a trusted Security Account, Antiope will leverage Cross Account Assume Roles
  to gather up resource data and store them in an inventory bucket. This bucket
  can then be index by ELK or your SIEM of choice to provide easy searching of
  resources across hundreds of AWS accounts.
* [Lambda Looter](https://github.com/StateFarmIns/LambdaLooter) will take a list
  of profiles and scan through them and download the code you have access to and
  then process that code for secrets outputting any potential secrets to a loot
  directory. Even though there can be a lot of false positives it makes looking
  for loot much faster than scanning the code itself.
* [Resource Utilization](https://github.com/aws-samples/resource-utilization) is
  just the first and most simple version of an account wide resource utilization
  metric & dashboard, to get overall insights on actually how much room for
  optimization is available. As a baseline, initial version considers only
  Amazon EC2 service CPU Utilization.
* [Protonizer](https://github.com/awslabs/protonizer) is a CLI tool for working
  with IaC in AWS Proton. Note that this is an experimental project and
  currently only supports generating Proton templates based on Terraform and
  CodeBuild provisioning. The tool also currently only supports primitive HCL
  data types such as strings, numbers, bools, and lists of primitive types. This
  is currently aligned with the Proton schema types that are supported by the
  Proton console.
* [AWS Service Enumeration](https://github.com/shabarkin/aws-enumerator) was
  created for service enumeration and info dumping for investigations of
  penetration testers during Black-Box testing. The tool is intended to speed up
  the process of Cloud review in case the security researcher compromised AWS
  Account Credentials. Yet another inventory tool!
* [Cloudstats](https://github.com/aristus/cloudstats) ETL works from the raw AWS
  Cost & Usage Report (CUR). It is implemented as a series of SQL statements
  with no out-of-band processing. The hope is that Cloudstats gives you a clean
  basis for your capacity planning and efficiency work. This work is based on
  experience with large AWS accounts over many years. Its accuracy is not
  guaranteed, but there are built-in ways to check its completeness and
  consistency. Via Corey Quinn.
* [CUDOS](https://aws.amazon.com/blogs/mt/visualize-and-gain-insights-into-your-aws-cost-and-usage-with-cloud-intelligence-dashboards-using-amazon-quicksight/)
  Dashboard is an in-depth, granular, and recommendation-driven dashboard to
  help customers dive deep into cost and usage and to fine-tune efficiency.
  Executives, directors, and other individuals within the CIO or CTO line of
  business or who manage DevOps and IT organizations will find the CUDOS
  Dashboard highly detailed and tailored to solve their use cases. Via 
* [Towards AWS](https://towardsaws.com/), perhaps modelled after Towards Data
  Science, is a blog where Where AWS Experts, Heroes, Builders, and Developers share
  their stories, experiences, and solutions.
* [AnsWerS](https://answersforaws.com/) is a yearly survey to collect data on
  the the state of AWS and related services. The data collected will be released
  publicly as a resource for all. Modelled after State of Serverless and State
  of Javascript.
* [AWS Icons](https://aws-icons.com/) allows you to download, copy and paste AWS
  icons in SVG and PNG format for your projects.
* [AWS Fargate Pricing Calculator](https://cloudtempo.dev/fargate-pricing-calculator)
  calculates your AWS Fargate costs based on region, CPU, memory, architecture,
  and storage options.
* [aws-service-auth-reference](https://github.com/fluggo/aws-service-auth-reference)
  is a JSON-formatted scrape of the
  [AWS Service Authorization Reference](https://github.com/fluggo/aws-service-auth-reference), along with a Golang program to update it. The package is updated weekly.
* [CloudFormation Captain
  Hook](https://catalog.us-east-1.prod.workshops.aws/workshops/d3460561-7264-4dbf-9587-2dd4b34306bf/en-US/introduction)
  promises no-code creation of CloudFormation hooks, using AWS CloudFormation
  GUard to validate resource properties, and storing rules in Amazon S3, AWS SSM
  Parameter Store, or the web. You need to whitelist your account for the beta
  to use it.
* [Substation](https://github.com/brexhq/substation) is a serverless data
  pipeline toolkit that integrates with AWS' most popular services (S3, Kinesis,
  API Gateway, DynamoDB, etc.), is optimized for affordability, and is used in
  production by the security team at Brex.
* [Amazon EC2 Image Builder Samples](https://github.com/aws-samples/amazon-ec2-image-builder-samples)
  contains samples that demonstrate various aspects of the Amazon EC2 Image
  Builder service. I will use this as a base for setting up an image pipeline
  using CloudFormation. I tried to build it once by hand and tripped up over the
  semantic version contraints.
* [SCPkit](https://github.com/aquia-inc/scpkit) provides a python module to aid
  in Service Control Policy management in AWS accounts. This tool will merge
  selected SCPs into the fewest amount of policies, and optionally remove
  whitespace characters as they count toward the byte limit.
* [specctl](https://github.com/awslabs/specctl) is a command-line based tool to
  extract and transform Kubernetes objects to ECS and vice versa. It has two
  modes, -m k2e (default) convert Kubernetes to ECS and -m e2k for ECS to
  Kubernetes.
* [AWS SigV4 Proxy](https://github.com/awslabs/aws-sigv4-proxy) will sign
  incoming HTTP requests and forward them to the host specified in the Host
  header. [Suggested as an alternative to awscurl](https://github.com/okigan/awscurl/issues/114).
* [CloudGoat](https://github.com/RhinoSecurityLabs/cloudgoat) is a training game,
Rhino Security Labs' "Vulnerable by Design" AWS deployment tool. It allows you
to hone your cloud cybersecurity skills by creating and completing several
"capture-the-flag" style scenarios. Each scenario is composed of AWS resources
arranged together to create a structured learning experience. Some scenarios are
easy, some are hard, and many offer multiple paths to victory. As the attacker,
it is your mission to explore the environment, identify vulnerabilities, and
exploit your way to the scenario's goal(s).
* [Athena SQLite Driver](https://github.com/dacort/athena-sqlite) uses Athena's
new Query Federation functionality to read SQLite databases from S3. Install it
from the Serverless Application Repository: AthenaSQLiteConnector.
* [Ruby on Jets](https://rubyonjets.com/) allows you to create and deploy
  serverless services with ease, and to seamlessly glue AWS services together
  with the most beautiful dynamic language: Ruby. It includes everything you
  need to build an API and deploy it to AWS Lambda. Jets leverages the power of
  Ruby to make serverless joyful for everyone.
* [cw](https://github.com/lucagrulla/cw) is the best way to tail AWS CloudWatch
  Logs from your terminal.
* [AwsMetricUrlHelper.py](https://github.com/daniel1302/aws_helpers/blob/master/AwsMetricUrlHelper.py)
  is code to generate CloudWatch URLs.
* [Fun with aws-vault and 1Password](https://austincloud.guru/2018/03/30/fun-with-aws-vault-and-1password/)
  shows code to plug 1password into aws vault.
* [SSM Supercharged](https://github.com/HQarroum/ssm-supercharged) AWS SSM
  integration with OpenSSH + EC2 Instance Connect + sshuttle. Looks like a
  competitor of aws-gate.
* [lsh](https://github.com/tobilg/lsh) is the Lambda shell, for running
  interactive commands on AWS Lambda. It's a CLI that can install a stack with a
  Lambda designed for interactive use.
* [The AWS Deployment Framework
  (ADF)](https://github.com/awslabs/aws-deployment-framework) is an extensive
  and flexible framework to manage and deploy resources across multiple AWS
  accounts and regions based on AWS Organizations. ADF allows for staged,
  parallel, multi-account, cross-region deployments of applications or resources
  via the structure defined in AWS Organizations while taking advantage of
  services such as AWS CodePipeline, AWS CodeBuild, and AWS CodeCommit to
  alleviate the heavy lifting and management compared to a traditional CI/CD
  setup. ADF allows for clearly defined deployment and approval stages which are
  stored in a centralized configuration file. It also allows for account based
  bootstrapping, by which you define an AWS CloudFormation template and assign
  it to a specific Organization Unit (OU) within AWS Organizations. From there,
  any account you move into this OU will automatically apply this template as
  its baseline.
* [s5cmd](https://github.com/peak/s5cmd) is a parallel S3 and local filesystem
  execution tool. 

## How to passively discover tools

Watch Donne Martin's [Awesome AWS](https://github.com/donnemartin/awesome-aws)
repo. It's basically a community-maintained, all-encompassing version of this
page. I don't intend this page to replicate the breadth of Donne's list.

Subscribe to Clint Gibler's [tldr;sec newsletter](https://tldrsec.com/).

[Danny Steenman](https://github.com/dannysteenman/aws-toolbox/) curates a list of AWS tools.

<!-- References -->

[aws-gate-pr]: https://github.com/xen0l/aws-gate/issues/712
[aws-cli-v2]: https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html
[aws-cli-debate]: https://github.com/aws/aws-cli/issues/4947
[saml2aws-pr]: https://github.com/Versent/saml2aws/issues/552