---
layout: post
title:  "Tools for technical writing"
date:   2023-05-24 17:00:00 +01:00
---

## Markdown links

[Link Checker](https://marketplace.visualstudio.com/items?itemName=dlyz.md-link-checker) validates local and external links. Also checks links to heading in local files.

[Link Updater](https://marketplace.visualstudio.com/items?itemName=mathiassoeholm.markdown-link-updater) automatically updates links when moving or renaming files in the workspace.

[Link Expander](https://marketplace.visualstudio.com/items?itemName=skn0tt.markdown-link-expander) creates pretty Markdown links using its HTML title.

[Link Completion Provider](https://marketplace.visualstudio.com/items?itemName=KlausJandl.markdownlinkcompletionprovider) suggests links to headers in the same file.

[Link Graph](https://marketplace.visualstudio.com/items?itemName=tchayen.markdown-links) displays a graph of local links.

[Link Suggestions](https://marketplace.visualstudio.com/items?itemName=TomasHubelbauer.vscode-markdown-link-suggestions) suggests local files and header anchors.

> This extension is made obsolete by the VS Code native feature called "Markdown path IntelliSense":
>
> https://code.visualstudio.com/updates/v1_64#_markdown-path-intellisense

Discover more [extensions for markdown links](https://marketplace.visualstudio.com/search?term=markdown%20link&target=VSCode&category=All%20categories&sortBy=Relevance).

## Markdown tables

[Table Prettifier](https://marketplace.visualstudio.com/items?itemName=darkriszty.markdown-table-prettify) Transforms tables to be more readable.

## Markdown Footnotes

[Footnotes](https://www.markdownguide.org/extended-syntax/#footnotes) are extended syntax of Markdown. VS Code by defualt ignores it.

[Markdown Footnote](https://marketplace.visualstudio.com/items?itemName=houkanshan.vscode-markdown-footnote) support footnote preview, jumping, peeking, inserting, and rendering.

[Markdown Footnotes](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-footnotes&ssr=false) Adds `[^footnote]` syntax support to VS Code's built-in markdown preview.

## Images

[Paste Image](https://marketplace.visualstudio.com/items?itemName=mushan.vscode-paste-image) pastes an image from clipboard directly.