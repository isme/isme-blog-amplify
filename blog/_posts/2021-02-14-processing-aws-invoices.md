---
layout: post
title: Processing AWS invoices
---

AWS sends a bunch of emails at the start of every month to advise that the
latest invoice is available.

If you have disabled "Receive PDF invoice by email" in the billing preferences,
you need to download the invoices through the AWS console.

To download the invoices for each organization:

* Log into the organization management account
* Go to the Billing console
* Go to Orders and Invoices
* Download each new invoice

Each account in the organization is invoiced separately.

The invoice file is named with the invoice identifier. For my own filing I
prefix that with the date of issue and the account ID. Get that info by viewing
the invoice.

To look up the name of each account, run this command in each organization
management account. Use the output to match account IDs to account names.

```bash
aws organizations list-accounts --profile ...
```
