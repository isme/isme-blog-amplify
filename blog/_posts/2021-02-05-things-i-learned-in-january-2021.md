---
layout: post
title: Things I learned in January 2021
---

This is the first in a series of what I hope to be monthly reviews of what I've
been working on and learning. My plan is to publish any reusable solutions I
create.

I created a pipeline in Bitbucket that deploys a CloudFormation stack to a test
account in AWS. It performs various security and quality checks in parallel
using cfn-lint, cfn_nag, CFRipper, yamllint, mdl. It does a full build-up and
tear-down of the stack with taskcat. Finally it does an incremental deploy of
the existing stack with sam. I build this to test a proof of concept with a
complex networking setup and I believe it's generally useful so I'll split it
out into a public project when I have time.

I used the above to set up a proof of concept for querying a MySQL RDS instance
in a private subnet using familiar desktop tools like mysql client and DBeaver.
The trick is to use Session Manager. [Charlie Bellmer's example][nullsweep] got
me started and later I [added the functionality](aws-gate-l) to xen0l's aws-gate.

[Pipelines](https://github.com/ktomk/pipelines) is a local pipeline runner for
Bitbucket pipelines. Need to test the setup of CloudFormation stack from scratch
in a pipeline? That can consume several of your precious build minutes. Save
them by running the pipeline locally before committing wasteful errors.

I recertified as an solutions architect associate. Thanks to Whizlabs for the
practice tests and to Adrian Cantrill for the video lectures.

I started learning Terraform. It has a wonderful module called
[aws-secure-baseline](https://github.com/nozaq/terraform-aws-secure-baseline) by
nozaq which can be used to automatically provision AWS Config across multiple
regions and accounts. AWS Config is a prerequisite for Security Hub, but doesn't
set itself up automatically.

AWSLambdaFullAccess also grants full access to S3(!). Use AWSLambda_FullAccess
instead. At the time of writing a Google search for the underscored version
preferred a
[Corey Quinn tweet](https://twitter.com/QuinnyPig/status/1341170740553342976)
over the official documentation buried in the
[Lambda troubleshooting guide](https://docs.aws.amazon.com/lambda/latest/dg/security_iam_troubleshoot.html#security_iam_troubleshoot-admin-deprecation).

---

The SNS create-topic API is idempotent. This can cause confusing results in
Terraform state files if you deploy the same resources to multiple accounts but
mess up the CLI profile for one of them so that you're deploying twice to the
same account.

I haven't checked how CloudFormation handles it, but it would make more sense to
me that the topic existence were checked before creating the topic.

You can create the same topic name as many times as you like.

```
$ for i in {1..3}; do aws sns create-topic --name ConfigChanges --profile saa-sso-iam --region eu-west-1; done
{
    "TopicArn": "arn:aws:sns:eu-west-1:638726906110:ConfigChanges"
}
{
    "TopicArn": "arn:aws:sns:eu-west-1:638726906110:ConfigChanges"
}
{
    "TopicArn": "arn:aws:sns:eu-west-1:638726906110:ConfigChanges"
}
```

[nullsweep]: https://nullsweep.com/a-better-way-to-ssh-in-aws/
[aws-gate-l]: https://github.com/xen0l/aws-gate/pull/713