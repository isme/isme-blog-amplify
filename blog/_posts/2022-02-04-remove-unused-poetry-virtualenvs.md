---
layout: post
title:  "Remove unused poetry virtualenvs"
date:   2022-02-07 21:25:52 2022 +0100
---

Where does Poetry keep its virtualenvs?

```
$ poetry config virtualenvs.path
/home/isme/.cache/pypoetry/virtualenvs
```

How much space are they taking up? (About 5% of my disk.)

```
$ du -sh "$(poetry config virtualenvs.path)"
7.5G	/home/isme/.cache/pypoetry/virtualenvs
```

How many virtualenvs do I have?

```
$ find "$(poetry config virtualenvs.path)" -mindepth 1 -maxdepth 1 -type d | wc -l
44
```

How many remain unchanged in the last 30 days? (The trick here is `-mtime +30`,
which means last modified more than 30 days ago.)

```
$ find "$(poetry config virtualenvs.path)" -mindepth 1 -maxdepth 1 -type d -mtime +30 | wc -l
37
```

Delete them!  (It uses `rm -rf` because the `-delete` action doesn't know how to delete a folder with contents. I don't know if `find` has syntax to delete the contents before deleting the folder.)

```
$ find "$(poetry config virtualenvs.path)" -mindepth 1 -maxdepth 1 -type d -mtime +30 -exec rm -rf {} \;
```

How many virtualenvs do I have now?

```
$ find "$(poetry config virtualenvs.path)" -mindepth 1 -maxdepth 1 -type d | wc -l
7
```

How much space are they taking up? About 20% of the previous amount :-)

```
$ du -sh "$(poetry config virtualenvs.path)"
1.4G	/home/isme/.cache/pypoetry/virtualenvs
```
