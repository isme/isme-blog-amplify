---
layout: post
title: Things I learned in February 2021
---

VS Code has a focusNoScroll option to fix a UI probelm that was destroying my
flow.

I swear it didn't use to do it, but VS Code has started annoyingly scrolling the
file list when you change editor tab. It's very distracting when you're trying
to read a directory tree, you close a tab, and the tree view moves to show
whatever the file for whatever editor tab gets selected next. Becuase I work in
many projects at the same time, that's usually in a different folder far away I
i completely lose my context. I end up having to collapse the whole tree and
find the file I wanted again.

Github issue: https://github.com/microsoft/vscode/issues/23902

Release note: https://code.visualstudio.com/updates/v1_46

---

If you need to use a non-default SSH key for access to a Git repo, you can now
use the core.sshCommand config setting to do it on a per-repo basis.

You might have to do this, for example, if you use Bitbucket for both personal
and client projects. Or if you already used your default key on a guest user in
Azure Devops while you were waiting for your real user to be created. After the
guest user is removed from the Azure Devops project there seems to be no way to
manage its SSH keys.

The last time I had to do this I had to create a custom SSH config and hack a
custom fake Host name like `bitbucket.org-personal` to make this work.

This is a much better solution.

From the [official docs](https://git-scm.com/docs/git-config):

core.sshCommand

If this variable is set, git fetch and git push will use the specified command
instead of ssh when they need to connect to a remote system. The command is in
the same form as the GIT_SSH_COMMAND environment variable and is overridden when
the environment variable is set.

So basically every client repo could have local settings like this:

```bash
git config --local core.sshCommand "ssh -i ~/.ssh/id_rsa_client -F /dev/null"
git config --local user.name "Iain Samuel McLean Elder"
git config --local user.email "iain@client.com"
```

If you're downloading the repo for the first time, you need to use this syntax
to set core.sshCommand at clone time.

```
git clone \
--config core.sshCommand="ssh -i ~/.ssh/id_rsa_client -F /dev/null" \
REPO_URL
```

Now when you list the repo's local settings you should see core.sshcommand
already set (note how the case differs from that documented).

```
$ git config --local --list | grep core.sshcommand
core.sshcommand=ssh -i ~/.ssh/id_rsa_dentsu -F /dev/null
```

---

Even with project administrator permissions in Azure Devops, you will not be
able to edit then wiki with only "stakeholder" access.

You need a user license to be able to make changes.

I discovered this through an error from git while trying to push to the repo
with my new user.

```
$ git push
remote: TF401027: You need the Git 'GenericContribute' permission to perform this action. Details: identity '12345678-1234-1234-1234-12345678\iain@client.com', scope 'repository'.
fatal: No se pudo leer del repositorio remoto.

Por favor asegúrate que tienes los permisos de acceso correctos
y que el repositorio existe.
```

The error talks about "contribute" permissions, but I was already in the administrators group.

The
[Azure DevOps Guide](https://www.azuredevopsguide.com/stakeholders-or-users-not-able-to-edit-azuredevops-wiki/)
has the answer.

> Even though User is part of Contributors groups, a user still needs basic
> license to edit the Wiki. And this is the exact reason why Stakeholder’s
> doesn’t have access"

---

You can set your multi-line-command shell prompt variable `PS2` to four spaces
so that multiline command executions pasted into the console are still copiable
and pastable from the terminal.

```text
$ export PS2="    "
$ eksctl create fargateprofile \
    --cluster demo-cluster \
    --name fp-example \
    --namespace example \
    --labels app=example
[ℹ]  creating Fargate profile "fp-example" on EKS cluster "demo-cluster"
[ℹ]  Either Fargate profile "fp-example" already exists on EKS cluster "demo-cluster" or another profile is being created/deleted, no action taken
```

---

Described my use case in a Github issue propising timestamps in eksctl logs. The issue was resolved with a PR the same day, and the next release should contain the enhancement.
https://github.com/weaveworks/eksctl/issues/3034

Opened an issue on the AWS CLI to request that the
`aws identitystore list-users` API list all users. Currently it only "lists" one
at a time. The SSO team is working on adding the functionality to their API.
https://github.com/aws/aws-cli/issues/5930

Provided feedback on Github issue to create apt packages for ekstl.
https://github.com/weaveworks/eksctl/issues/215

---

Did someone edit the Azure Devops wiki at the same time as you? Your next push
will fail.

Here's a git workflow to fix it.

```bash
git branch fix
git reset --hard HEAD~1
git pull
git cherry-pick fix
git push
git branch -D fix
```

---

Why does my ingress have no address and no ALB?

```
$ kubectl get ingress -n gscm-auth-srv
NAME                    CLASS    HOSTS   ADDRESS   PORTS   AGE
gscm-auth-srv-ingress   <none>   *                 80      73s
$ kubectl describe ingress -n gscm-auth-srv
Name:             gscm-auth-srv-ingress
Namespace:        gscm-auth-srv
Address:          
Default backend:  default-http-backend:80 (<error: endpoints "default-http-backend" not found>)
Rules:
  Host        Path  Backends
  ----        ----  --------
  *           
              /*   gscm-auth-srv:80 (192.168.113.65:8880)
Annotations:  alb.ingress.kubernetes.io/scheme: internet-facing
              alb.ingress.kubernetes.io/target-type: ip
              kubernetes.io.ingress.class: alb
Events:       <none>
```

Spot the difference.

```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
 namespace: game-2048
 name: ingress-2048
 annotations:
 kubernetes.io/ingress.class: alb
 alb.ingress.kubernetes.io/scheme: internet-facing
 alb.ingress.kubernetes.io/target-type: ip
```
 
```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
 name: gscm-auth-srv-ingress
 namespace: gscm-auth-srv
 annotations:
 kubernetes.io.ingress.class: alb
 alb.ingress.kubernetes.io/scheme: internet-facing
 alb.ingress.kubernetes.io/target-type: ip
```

kubectl does not validate annotations.

---

Terminal ignoring backslashes of a multi-line command you copied and pasted?
Check the line ending settings. Bash silently ignores CRLF.

---

Google Maps' rendering of political borders depends on who's viewing the map.
This came up when some Morrocan colleagues ask us Europeans why "their country
was split in half". We were using Google Maps API results in internal software.
The results were rendered from a request made in Europe, where Western Sahara
(the bottom half of Morroco) is maps a "disputed boundary".

https://support.google.com/maps/answer/3145721?hl=en

---

How do you find out which package a command is in?

Some commands are in a packaged suite.

Apt is no help by itself and sometimes the manual doesn't tell you.

```
$ apt show xdg-mime
N: No se ha podido localizar el paquete xdg-mime
N: No se ha podido localizar el paquete xdg-mime
E: No se encontró ningún paquete
$ man xdg-mime
```

[Dpkg can tell you, though](https://askubuntu.com/a/482/143624).

```
$ dpkg -S xdg-mime
xdg-utils: /usr/share/man/man1/xdg-mime.1.gz
bash-completion: /usr/share/bash-completion/completions/xdg-mime
google-chrome-stable: /opt/google/chrome/xdg-mime
xdg-utils: /usr/bin/xdg-mime
```

---

I wrote an installation script for Anki and a Github pipeline to test it.

https://github.com/iainelder/dotfiles/actions/runs/584452259

---

I learned how to install SnowSQL and configure the CLI to run basic queries.

---

I subscribed to the AWS Security Bulletins RSS feed.

---

Learning from Corey Quinn's newsletter.

Book recommendations from
[Andreas Wittig](https://cloudonaut.io/5-good-reasons-not-to-get-aws-certified/):

https://gumroad.com/l/aws-good-parts
https://www.dynamodbbook.com/
https://www.manning.com/books/serverless-architectures-on-aws-second-edition
https://awsmaniac.com/

[xssfox](https://sprocketfox.io/xssfox/about/) reminds is not to use the
branch's own code to build what's in the branch. The buildspec.yml and the
deploy.sh could be modified by a malicious user.
https://sprocketfox.io/xssfox/2021/01/18/pipeline/

As a solo independent consultant I can't become an AWS partner, but
[Mike from Duckbill](https://www.duckbillgroup.com/blog/protecting-our-clients-incentive-alignment/)
made me think twice about it. "Once you’re in a partner program, your loyalties
become divided between the client and the partner. As we want our incentives to
be unquestionable, we knew that we could never partner with anyone or any
company. Our independence ensures alignment."

https://www.duckbillgroup.com/blog/protecting-our-clients-incentive-alignment/

Rediscovered the Amazon Builder's Library through this excellently written
article by Colm MacCárthaigh.

https://aws.amazon.com/builders-library/reliability-and-constant-work/

You can run Wordpress as a Lambda now!
https://github.com/apparentorder/reweb

---

To edit a service connection in an Azure Devops, at least in my environment, you
have to follow a strange sequence of steps after the obvious thing fails the
first time. I discovered a repro and reported in the Microsoft Developer
Community. The bug has been confirmed and triaged by support.

---

I learned to use L2TP/IPSec in Ubuntu. You need to install the
network-manager-l2tp-gnome package to use it.

---

Need to find an IAM user by its access key ID? Use the
`aws sts get-access-key-info` API to look up the account ID of the access key
and then use the IAM console in the account to search by access key ID.