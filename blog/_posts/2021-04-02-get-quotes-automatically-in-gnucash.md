---
layout: post
title:  "Get Quotes automatically in GnuCash"
date:   2021-04-02 12:44:00 +02:00
---

I was reading the [GnuCash Tutorial and Concepts Guide][gnccur] to learn about
handling multiple currencies (GBP and EUR).

It says you can import exchange rates automatically, but it's missing the
following information before the "Get Quotes" button does its job:

* Register for a personal API key from the [Alpha Vantage][av] website
* Navigate to GnuCash's online quotes preferences (Edit > Preferences > Online Quotes)
* Paste the API key into the Alpha Vantage API key field

As a bonus, you can now script currency conversions using gnc-fq-dump. The Alpha
Vantage API key is [stored in dconf][gncdconf].

```bash
$ export ALPHAVANTAGE_API_KEY="$(dconf read /org/gnucash/general/finance-quote/alphavantage-api-key)"
$ gnc-fq-dump currency GBP EUR
1 GBP = 1.17502 EUR
```

# Investigation

The tutorial says only this:

> ### 12.2.2.2. Automatic Updating Exchange Rates (How-To)
> 
> Open the Price Editor by going to Tools → Price Editor.
>
> Click on the Get Quotes button to automatically load the various exchange
> rates you need.

I try that and get an error:

> Unable to retrieve quotes for these items:  
> CURRENCY:GBP

I searched on Google for [gnucash unable to retrieve quotes for these items] and
found the same error reported by Paul Annekov on
[Personal Finance and Money Stack Exchange][pfmse].

Paul's problem was requesting more currencies than the Alpha Vantage API allows.
But I am requesting only one.

The Alpha Vantage API is not mentioned in the tutorial's currency page.

Anyway, Paul says this:

> What I tried:
> 
> * I set up Alpha Vantage API key in Preferences - Online Quotes

[GnuCash's Online Quotes wiki page][gncwiki] has more info about this.

> Finance::Quote's alphavantage and currency sources require an AlphaVantage API
> key that you can get free from the [AlphaVantage Website][av]. Command-line
> Helper Scripts like gnc-fq-dump will obtain the key from the
> ALPHAVANTAGE_API_KEY environment variable. GnuCash will too if it uses the
> same environment.
> 
> For GnuCash to be able to use Online Quotes, if the API key is not already
> specified in the environment, it can be entered in
> 
> * GnuCash versions 2.6.20/2.7.4 and newer: Online Quotes tab of GnuCash
> Preferences

After navigating all this, I got the the solution which you read at the top of
the page.

<!-- References -->

[gnccur]: https://www.gnucash.org/docs/v4/C/gnucash-guide/currency_manual.html
[gncdconf]: https://www.gnucash.org/docs/v4/C/gnucash-guide/basics-migrate-settings.html
[pfmse]: https://money.stackexchange.com/questions/107040/unable-to-retrieve-quotes-in-the-modern-gnucash-with-alpha-vantage
[gncwiki]: https://wiki.gnucash.org/wiki/Online_Quotes
[av]: https://www.alphavantage.co/
