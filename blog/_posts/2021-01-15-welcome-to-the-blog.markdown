---
layout: post
title:  "Welcome to the blog!"
date:   2021-01-15 13:01:22 +0100
---

This is really just a placeholder post until I have something to write about.

See the [README][readme] on Bitbucket for how I set up this site.

I've included some of the videos I found helpful while researching AWS Amplify,
my hosting provider.

"I'm not a front-end developer, so ..." is a phrase you might here in these
videos. It describes me too, and so I am grateful to these other backend
developers for taking the time to show us how it's done.

[Marcia Villalba uses Amplify to host a static website][marcia-frontend]. This
walked me through the whole process end to end and gave me the confidence to
launch my site. It gives demos for Hugo and React, but and I figured out the
build steps for Jekyll myself.

[Marcia Villalba again taking it further, using Amplify to add backend services][marcia-backend].
I've not watched this yet, but I might need it soon.

[Heitor Lessa uses Amplify to build a whole really complex serverless app for a
fictional airline][heitor-complex]. This is a lot more complex than I need for this site, but he
did recommend some useful UI design tools.

* collectui.com - for categorized inspiration from UIs in the wild
* coolors.co - for generating color palettes
* invisionapp.com - for storyboarding / wireframing
* html5up.net - for pretty HTML templates

[readme]: https://bitbucket.org/isme/isme-blog-amplify/src/master/README.md
[marcia-frontend]: https://www.youtube.com/watch?v=fHK6e6en0YA
[marcia-backend]: https://www.youtube.com/watch?v=SKOdTUtEuZo
[heitor-complex]: https://www.youtube.com/watch?v=DcrtvgaVdCU
