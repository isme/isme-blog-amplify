---
layout: post
title:  "How to use AWS SSO profiles"
date:   2021-03-30 12:47:00 +02:00
---

I wrote this post because lately I've been introducing developers to using AWS
SSO for temporary credentials.

I've listed all the languages and tools used by a current client. There are
obviously many more! If I discover the AWS SSO support version for others I'll
add them here.

SSO support is still quite a new feature, so other tools that depend on the AWS
SDKs are just starting to adopt it.

# AWS CLI

The AWS CLI v2 supports SSO profiles since 2.0.0 GA.

It's the easiest way to write SSO profile configuration because it has a step by
step process in the CLI. (`aws configure sso`).

It's the only tool I'm aware of that can renew the SSO credential cache
(`aws sso login`).

https://aws.amazon.com/es/blogs/developer/aws-cli-v2-now-supports-aws-single-sign-on/

https://aws.amazon.com/es/blogs/developer/aws-cli-v2-is-now-generally-available/

# .NET

The .NET SDK supports SSO profiles since
[v3.5.93.0](https://github.com/aws/aws-sdk-net/blob/master/SDK.CHANGELOG.md#35930-2021-01-20-0024-utc).

> SDK.CHANGELOG.md
> 
> 3.5.93.0 (2021-01-20 00:24 UTC)
>
> * Core 3.5.2.0
> 
>     * [...] AWS SSO based Credential Profiles are now handled by the SDK.

The AWSSDK.Core component supports SSO profiles since
[v3.5.2](https://github.com/aws/aws-sdk-net/pull/1701#issuecomment-763271274).

> normj commented on 20 Jan
> 
> This has been squashed merged and released in version 3.5.2 of AWSSDK.Core

# Go

AWS SDK for Go v2 supports SSO profiles since
[v1.1.0](https://github.com/aws/aws-sdk-go-v2/releases/tag/v1.1.0). Support was
improved in [v1.2.0](https://github.com/aws/aws-sdk-go-v2/releases/tag/v1.2.0).

AWS SDK for Go v1 supports SSO profiles since
[v.1.37.0](https://github.com/aws/aws-sdk-go/releases/tag/v1.37.0). Support was
improved in [v.1.37.2](https://github.com/aws/aws-sdk-go/releases/tag/v1.37.2).

# Terraform

Terraform S3 backend supports SSO profiles since
[v0.14.6](https://github.com/hashicorp/terraform/releases/tag/v0.14.6).

Terraform AWS provider supports SSO profiles since
[v3.26.0](https://github.com/hashicorp/terraform-provider-aws/releases/tag/v3.26.0).
Support was improved in
[v3.27](https://github.com/hashicorp/terraform-provider-aws/releases/tag/v3.27.0).
