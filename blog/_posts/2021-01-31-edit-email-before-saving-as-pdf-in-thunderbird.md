---
layout: post
title: Edit email before saving as PDF in Thunderbird
---

Thunderbird doesn't appear to have a built in way to do this, but you can do it
with other tools vailabile in Ubuntu 20.

* Select email in Thunderbird inbox
* Export as .eml (File > Save as > File)
* Open the .eml file in a text editor
* Edit the email to correct any formatting or add any missing details
* Convert the email to HTML with [MHonArc](https://www.mhonarc.org/) (Thanks to [3wordchant on Ask Ubuntu](https://askubuntu.com/a/1290140/143624) for the tip.)

    ```
    sudo apt install mhonarc
    mhonarc -single file.eml > file.html
    ```

* Open the .html file in a text editor
* Edit the HTML to remove the headers

    * Select all lines inclusive

        * from  `<!--X-Head-Body-Sep-Begin-->`
        * to `<!--X-Head-Body-Sep-End-->`

    * Delete selected lines

* Open .html file in Firefox
* Print and save as PDF
