---
layout: default
title: Terraform Tools
---

I'm learning Terraform.

This is just a list of stuff that might help me to work with it better.

* [Prettyplan](https://github.com/chrislewisdev/prettyplan) help you view large
  Terraform plans with ease. Unecessary as of Terraform 0.12.
* [Infracost](https://github.com/infracost/infracost) shows cloud cost estimates
  for Terraform projects.
* [driftctl](https://github.com/cloudskiff/driftctl) tracks how well your IaC
  codebase covers your cloud configuration. driftctl warns you about drift.
* [tflint](https://github.com/terraform-linters/tflint). A Pluggable Terraform
  Linter. Finds possible errors for major cloud providers, warns about
  deprecated syntax and unused declarations, enforces best practices and naming
  conventions.
* [Terraform Parliament](https://github.com/rdkls/tf-parliament) Parliamente for
  Terraform. Doesn't support S3 bucket policies yet.
* https://github.com/gruntwork-io/terratest/
* https://terraform-compliance.com/
* https://github.com/stelligent/config-lint
* [Terragrunt](https://terragrunt.gruntwork.io/) is a thin wrapper that provides
  extra tools for keeping your configurations DRY, working with multiple
  Terraform modules, and managing remote state.
*
  [Terraform AWS Secure Baseline](https://github.com/nozaq/terraform-aws-secure-baseline)
  is a module to set up your AWS account with the secure baseline configuration
  based on CIS Amazon Web Services Foundations and AWS Foundational Security
  Best Practices.
* https://learn.hashicorp.com/tutorials/terraform/cdktf


* [AirIAM](https://github.com/bridgecrewio/AirIAM) is an AWS IAM to least
  privilege Terraform execution framework. It compiles AWS IAM usage and
  leverages that data to create a least-privilege IAM Terraform that replaces
  the exiting IAM management method.
* [tfsec](https://github.com/tfsec/tfsec) uses static analysis of your terraform
  templates to spot potential security issues
* [Terrascan](https://github.com/accurics/terrascan) detects security
  vulnerabilities and compliance violations across your Infrastructure as Code
* [Atlantis](https://github.com/runatlantis/atlantis) runs terraform plan and
  apply remotely and comments back on the pull request with the output.
*
  [Terraformer](https://github.com/GoogleCloudPlatform/terraformer#use-with-aws)
  generates tf/json and tfstate files based on existing infrastructure (reverse
  Terraform).
*
  [Terragrunt example infrastructure](https://github.com/gruntwork-io/terragrunt-infrastructure-live-example)
  shows an example of how to use modules without duplicating any of the
  Terraform code.
* [kics](https://github.com/Checkmarx/kics) keeps cloud infrastructure secure.
  Find security vulnerabilities, compliance issues, and infrastructure
  misconfigurations early in the development cycle of your
  infrastructure-as-code.
* [Checkov](https://github.com/bridgecrewio/checkov/)
* [Terrahub](https://github.com/tfxor/terrahub) is enterprise friendly GUI to
  show realtime terraform executions, as well as auditing and reporting
  capabilities for historical terraform runs.
* [tool-compare](https://github.com/iacsecurity/tool-compare) A test suite to
  compare Checkov, Indeni Cloudrail, Kics, Snyk, Terrascan, and Tfsec.
* [tfquery](https://github.com/mazen160/tfquery) Run SQL queries on yout
  Terraform infrastructure.
* [tfnotify](https://github.com/mercari/tfnotify) parses Terraform commands'
  execution result and applies it to an arbitrary template and then notifies it
  to GitHub comments etc.
* [Terraformer](https://github.com/GoogleCloudPlatform/terraformer) is a CLI
  tool that generates tf/json and tfstate files based on existing infrastructure
  (reverse Terraform).
* [yor](https://github.com/bridgecrewio/yor) is an open-source tool that helps
  add informative and consistent tags across infrastructure-as-code frameworks
  such as Terraform, CloudFormation, and Serverless.
* [Confectionery](https://github.com/Cigna/confectionery) is a library of rules
  for the Conftest tool. These rules can be used to detect misconfigurations in
  Terraform plans and other configuration file formats. The terraform rules also
  leverage the Regula library to assist with the parsing of Terraform plans.
* [Terve](https://github.com/superblk/terve) is a unified, minimal terraform and
  terragrunt version manager. A possible replacement for tgenv and tfenv.
* [hcledit](https://github.com/minamijoyo/hcledit) reads HCL from stdin, edits
  and writes to stdout, easy to pipe and combine with other commands. HCL
  (Hashicorp Configuration Language) is the language used to write Terraform
  configuration. Terraform also accepts JSON input, but this could be helpful
  for manipulating the more familiar format.
* [OTS: Open Terraforming Server](https://github.com/leg100/ots) is a prototype
  open source alternative to Terraform Enterprise.
* [tfmigrator CLI](https://github.com/tfmigrator/cli) is a CLI to migrate
  Terraform Configuration and state.
* [Brainboard](https://www.brainboard.co/) "is the one and only tool to visually build your cloud infrastructures and deploy your auto-generated Terraform code in less than an 1-hour."
* [Terravalet](https://github.com/Pix4D/terravalet) generate migration scripts
  for low-level Terraform operations such as renaming resources in the same
  state, moving resources between states, and importing resources into a state.
* [tfedit](https://github.com/minamijoyo/tfedit) provides Easy refactoring
  Terraform configurations in a scalable way. Although the initial goal of this
  project is providing a way for bulk refactoring of the aws_s3_bucket resource
  required by breaking changes in AWS provider v4, but the project scope is not
  limited to specific use-cases.
* [Pike](https://github.com/JamesWoolfenden/pike) is a tool for determining the permissions or policy required for IAC code.
* [Announcing CDK for Terraform on AWS](https://aws.amazon.com/blogs/opensource/announcing-cdk-for-terraform-on-aws/) allows developers to set up their infrastructure as code without context switching from their familiar programming language, using the same tooling and syntax to provision infrastructure resources as they are using to define the application business logic. Teams can collaborate in familiar syntax, while still leveraging the power of the Terraform ecosystem and deploying their infrastructure configurations via established Terraform deployment pipelines.
* [Tighten your package security with CodeArtifact Package Origin Control toolkit](https://aws.amazon.com/es/blogs/devops/tighten-your-package-security-with-codeartifact-package-origin-control-toolkit/) shows how to defend against “dependency substitution“ or “dependency confusion” attacks when using AWS CodeArtifact.
*
  [IAM Policy Validator for Terraform](https://github.com/awslabs/terraform-iam-policy-validator)
  is A command line tool that validates AWS IAM Policies in a Terraform template
  against AWS IAM best practices.
* [hcl2json](https://github.com/tmccombs/hcl2json) is a tool to convert from HCL
  to json, to make it easier for non-go applications and scripts to process HCL
  inputs (such as terraform config). There are many projects with the same name.
  This is the most up-to-date one on Github.
* [terraform-docs](https://github.com/terraform-docs/terraform-docs) is a
  utility to generate documentation from Terraform modules in various output
  formats.
* [specctl](https://github.com/awslabs/specctl) is a command-line based tool to
  extract and transform Kubernetes objects to ECS and vice versa. It has two
  modes, -m k2e (default) convert Kubernetes to ECS and -m e2k for ECS to
  Kubernetes.