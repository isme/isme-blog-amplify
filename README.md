# isme.es Website and blog

This is the source code for the isme.es website and blog.

## Creating a new post

Check prerequistes in "Creating From Scratch".

The following is taken from Jekyll's documentation on [Posts][jd-posts]. Read
there for more info.

To create a post, add a file to your `_posts` directory with the following
format:

```
YEAR-MONTH-DAY-title.MARKUP
```

Where `YEAR` is a four-digit number, `MONTH` and `DAY` are both two-digit
numbers, and `MARKUP` is the file extension representing the format used in the
file. For example, the following are examples of valid post filenames:

```
2011-12-31-new-years-eve-is-awesome.md
2012-09-12-how-to-write-a-blog.md
```

All blog post files must begin with [front matter](/docs/front-matter/) which is
typically used to set a [layout](/docs/layouts/) or other meta data. For a simple
example this can just be empty:

```markdown
---
layout: post
title:  "Welcome to Jekyll!"
---

# Welcome

**Hello world**, this is my first Jekyll blog post.

I hope you like it!
```

[jd-posts]: https://jekyllrb.com/docs/posts/

## Creating From Scratch

Hugo was making me miserable, so I tried again with Jekyll.

Prerequisite: Rbenv needs to be installed so that the Ruby version can be
controlled by the .ruby-version file in the repo.

This repo was initalized like this:

```bash
bundle init
bundle add jekyll
bundle exec jekyll new blog
cd blog
bundle install
```

An site was set up manually in the AWS Amplify console. The site deploys
automatically on every push to the master branch.

The builds were iterated until a correct build specification was found. AWS
Amplify attempts to detect the settings automatically, but didn't get it
completely right in my case.

I downloaded the manually crafted amplify.yml and added it to the repo.

## Local testing

```bash
cd blog
bundle exec jekyll serve --watch
```

## Modify the minima theme

Jekyll uses the minima theme by default.

It's not quite minimal enough for me. It includes a footer on every page with
the same info that I would just include on the front page. Right now my website
is little more than a digital business card.


I made a copy of the minima theme files so I could copy them.

```bash
minima=$(bundle info --path minima)
cp -r "$minima"/{_includes,_layouts} .
```

After fiddling with the theme files for a while, I realised all I needed to do
was add an empty file at _inclyudes/footer.html.

## Deploy to production

To deploy to production, push changes to the git remote in Bitbucket.

AWS Amplify automates the deployment process when it detects changes in the remote repo.

Amplify has an access key registered in the repo's settings in Bitbucket.

The Amplify resources are hosted in "AWS Personal" eu-west-1. The app name is isme-blog-amplify.
